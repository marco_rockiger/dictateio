from setuptools import find_packages, setup

setup(
    name='dictateio',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_require=[
        'flask',
        'flask-migrate',
        'flask-shell-ipython',
        'flask-sqlalchemy'
    ]
)
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, HiddenField
from wtforms.validators import DataRequired

PLACEHOLDERTEXT = "Click on the »Start Recording« button or press »Alt+S« and begin speaking. After your finished dictating, your text will be spellchecked and you can edit it to your liking."


class CreateForm(FlaskForm):
    title = StringField(
        'title',
        validators=[DataRequired()], 
        render_kw={
            "placeholder": "Insert your title", 
            'spellcheck': 'true'})
    body = TextAreaField(
        'body',
        render_kw={
            'spellcheck': 'true',
            'placeholder': PLACEHOLDERTEXT
        })
    dialect = HiddenField(default="en-US")


class UpdateForm(FlaskForm):
    title = StringField(
        'title',
        validators=[DataRequired()],
        render_kw={
            "placeholder": "Insert your title", 
            'spellcheck': 'true'})
    body = TextAreaField(
        'body',
        render_kw={
            'spellcheck': 'true',
            'placeholder': PLACEHOLDERTEXT
        })
    dialect = HiddenField(default="en-US")
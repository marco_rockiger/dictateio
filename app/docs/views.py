from datetime import datetime

from flask import (flash, g, redirect, render_template, request,                      url_for)

from werkzeug.exceptions import abort

from app import db
from app.models import User, Doc, Replacement, dialects
from app.auth import login_required
from app.helpers import unsalt

from . import docs
from .forms import CreateForm, UpdateForm

@docs.route('/mydocs')
@login_required
def mydocs():
    docs = Doc.query.order_by(Doc.edited.desc()).filter_by(author_id=g.user.id)
    g.route = '/docs'
    return render_template('docs/index.html', docs=docs)

@docs.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    current_user = g.user
    form = CreateForm()
    replacements = get_replacements(current_user)
    if form.validate_on_submit():
        title = request.form['title']
        body = request.form['body']
        dialect = request.form['dialect']
        error = None

        if not title:
            error = 'A title is required.'

        if dialect not in dialects:
            dialect = current_user.prefered_dialect

        if error is not None:
            flash(error)
        else:
            new_post = Doc(
                title = title,
                body = body,
                author = current_user,
                dialect = dialect
            )
            db.session.add(new_post)
            db.session.commit()
            return redirect(url_for('docs.mydocs'))
    form.dialect.data = current_user.prefered_dialect

    return render_template('docs/create.html', form=form, hash=unsalt(current_user.password), email=current_user.email, author_id=current_user.id, replacements=replacements)
    

@docs.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    current_user = g.user
    post = get_post(id)
    replacements = get_replacements(current_user)
    form = UpdateForm()
    if form.validate_on_submit():
        title = request.form['title']
        body = request.form['body']
        dialect = request.form['dialect']
        error = None

        if not title:
            error = 'A title is required.'

        if dialect not in dialects:
            dialect = post.dialect or current_user.prefered_dialect

        if error is not None:
            flash(error)
        else:
            post.title = title
            post.body = body
            post.dialect = dialect
            post.edited = datetime.utcnow()
            db.session.commit()
            return redirect(url_for('docs.mydocs'))
    form.title.data = post.title
    form.body.data = post.body
    form.dialect.data = post.dialect

    return render_template('docs/update.html', post=post, form=form, hash=unsalt(current_user.password), email=current_user.email, id=id,author_id=post.author_id, replacements=replacements)


@docs.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    post = get_post(id)
    db.session.delete(post)
    db.session.commit()
    flash("Entry deleted")
    return redirect(url_for('docs.mydocs'))


def get_post(id, check_author=True):
    """Helper function that returns the post for the given id,
    optionally it checks if the post-author is the current user."""
    current_user = g.user
    post = Doc.query.filter_by(id=id).first()

    if post is None:
        abort(404, "Doc id {0} doesn't exist.".format(id))

    if check_author and post.author != current_user:
        abort(403)

    return post

def get_replacements(user):
    """Helper function that returns the replacements of the given user."""
    replacements_query = Replacement.query.filter_by(author_id=user.id).order_by().all()
    replacements = []
    for entry in replacements_query:
        replacements.append([entry.id, entry.placeholder, entry.substitute])
    return replacements
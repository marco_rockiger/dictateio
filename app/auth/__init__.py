import functools

from flask import Blueprint, g, redirect, url_for

auth = Blueprint('auth', __name__, url_prefix='/auth')

from . import views


def login_required(view):
    """A view decorator to check if the user is logged in"""
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        
        return view(**kwargs)
    
    return wrapped_view
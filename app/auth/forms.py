from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo
from wtforms import ValidationError
from app.models import User


def primaryButton(txt):
    return '<button type="submit" class="ui fluid large primary submit button">%s</button>' % (txt)


class LoginForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[DataRequired(), Length(1, 64), Email()],
        render_kw={"placeholder": "Email address"})
    password = PasswordField(
        'Password',
        validators=[DataRequired()],
        render_kw={"placeholder": "Password"})
    submit = primaryButton("Log In")



class ProfileForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[
            DataRequired(),
            Length(1, 64),
            Email()],
        render_kw={"placeholder": "Email address"})
    username = StringField(
        'Username',
        validators=[
            DataRequired(),
            Length(1, 64),
            Regexp('^[A-Za-z][A-Za-z0-9_. ]*$', 0,
                   'Names must have only letters, numbers, dots or underscores')],
        render_kw={"placeholder": "Name"})

    submit = primaryButton("Register")

    def validate_email(self, field): # validates the email field because of validate_* plus email
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

    def validate_username(self, field): # validates the email field because of validate_* plus username
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use.')



class RegisterForm(ProfileForm):
    password = PasswordField('Password', validators=[
        DataRequired(),
        EqualTo('password2', message='Passwords must match.')],
        render_kw={"placeholder": "Password"})
    password2 = PasswordField('Confirm password', validators=[DataRequired()],
        render_kw={"placeholder": "Repeat password"})

class EditProfileForm(ProfileForm):
    pass

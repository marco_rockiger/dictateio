from flask import (redirect, g, render_template,
                   request, session, url_for, flash)
from flask_login import login_user, logout_user, current_user, login_required

from werkzeug.security import check_password_hash, generate_password_hash

from app import db
from app.models import User, Doc
from app.decorators import admin_required

from . import auth
from .forms import primaryButton, LoginForm, RegisterForm, EditProfileForm
from .tutorialtext import TUTORIAL_TITLE, TUTORIAL_BODY
from ..email import send_email


@auth.route('/register', methods=('GET', 'POST'))
#@admin_required
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(email=form.email.data,
                    username=form.username.data,
                    password=generate_password_hash(form.password.data))
        db.session.add(user)
        db.session.commit()

        token = user.generate_confirmation_token()
        send_email(user.email, 'Welcome to Dictandu',
                   'auth/email/confirm', user=user, token=token)

        new_post = Doc(
            title = TUTORIAL_TITLE,
            body = TUTORIAL_BODY,
            author = user,
            dialect = 'en-US'
        )
        db.session.add(new_post)
        db.session.commit()
        flash('A confirmation email has been sent to you by email. You can now login.')
        return redirect(url_for('auth.login'))

    return render_template('auth/register.html', form=form)

@auth.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('pages.index'))
    if current_user.confirm(token):
        db.session.commit()
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('pages.index'))

@auth.route('/profile', methods=('GET', 'POST'))
@login_required
def edit_profile():
    form = EditProfileForm()
    form.email.data = current_user.email
    form.username.data = current_user.username
    if form.validate_on_submit():
        current_user.email = form.email.data
        current_name.username = form.username.data
        db.session.db(current_user._get_current_object())
        db.session.commit()
        flash('Your profile has been updated')
        return redirect(url_for('docs.index'))

    return render_template('auth/editProfile.html', form=form)


@auth.route('/login', methods=('GET', 'POST'))
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and check_password_hash(user.password, form.password.data):
            login_user(user, remember=True)
            next = request.args.get('next')
            if next is None or not next.startswith('/'):
                next = url_for('index')
            return redirect(next)
        flash('Incorrect username or password')
    return render_template('auth/login.html', form=form)

@auth.route('/logout')
def logout():
    logout_user()
    flash("You have been logged out.")
    return redirect(url_for('index'))


@auth.route('/signup')
def signup():
    return render_template('auth/signupbeta.html')


# to check before each request that the user is logged in
@auth.before_app_request
def before_request():
    user_id = session.get('user_id')
    if user_id is None:
        g.user = None
    else:
        g.user = User.query.filter_by(id=user_id).first()

    if current_user.is_authenticated:
        current_user.ping()

# to reset the last_seen value for the user
# @auth.before_app_request
# def before_request():
#     print("before_request")
#     if current_user.is_authenticated:
#         current_user.ping()
#         if not current_user.confirmed \
#                 and request.endpoint \
#                 and request.blueprint != 'auth' \
#                 and request.endpoint != 'static':
#             return redirect(url_for('auth.unconfirmed'))

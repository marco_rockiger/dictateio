import os.path, json, base64, os

from datetime import datetime, timedelta

from flask import url_for, current_app
from flask_login import UserMixin, AnonymousUserMixin
from itsdangerous import JSONWebSignatureSerializer as Serializer

from app import db
from . import login_manager
from .models_mixins import PaginatedAPIMixin


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.Text, unique=True, nullable=False, index=True)
    password = db.Column(db.Text, nullable=False)

    prefered_dialect = db.Column(db.String(20), default='en-US')
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow)
    
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    docs = db.relationship('Doc', backref='author', lazy='dynamic')

    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    confirmed = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<User %r>' % self.username

    def can(self, perm):
        return self.role is not None and self.role.has_permission(perm)

    def is_administrator(self):
        return self.can(Permission.ADMIN)

    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def get_token(self, expires_in=3600):
        now=datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    def generate_confirmation_token(self):
        s = Serializer(current_app.config['SECRET_KEY'])
        return s.dumps({'confirm': self.id}).decode('utf-8')

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token.encode('utf-8'))
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

class AnonymousUser(AnonymousUserMixin):
    def can(self, perm):
        return False

    def is_administrator(self):
        return False

login_manager.anonymous_user = AnonymousUser



class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        try: 
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()
        except AttributeError:
            pass

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    @staticmethod
    def insert_roles():
        roles = {
            'User': [Permission.DICTATE],
            'Moderator': [Permission.DICTATE, Permission.MODERATE],
            'Administrator': [Permission.DICTATE, Permission.MODERATE, Permission.ADMIN]
        }
        default_role = 'User'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permissions()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()



class Doc(PaginatedAPIMixin, db.Model):
    __tablename__ = 'docs'
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    edited = db.Column(db.DateTime, default=datetime.utcnow)
    title = db.Column(db.Text, nullable=False)
    body = db.Column(db.Text, nullable=False)
    dialect = db.Column(db.String(20))

    def __repr__(self):
        return '<Doc %r>' % self.title

    def to_dict(self):
        data = {
            'id': self.id,
            'author_id': self.author_id,
            'created': self.created.isoformat() + 'Z',
            'edited': self.edited.isoformat() + 'Z',
            'title': self.title,
            'body': self.body,
            'dialect': self.dialect,
            '_links': {
                'self': url_for('api.get_doc', id=self.id),
                'user': url_for('api.get_author', id=self.id)
            }
        }
        return data

    def from_dict(self, data):
        for field in ['title', 'body', 'dialect', 'author_id']:
            if field in data:
                setattr(self, field, data[field])


class Replacement(PaginatedAPIMixin, db.Model):
    __tablename__ = 'replacements'
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    edited = db.Column(db.DateTime, default=datetime.utcnow)
    placeholder = db.Column(db.String(100), nullable=False)
    substitute = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return '<Replacement %s:%s>' % (self.placeholder, self.substitute) 

    def to_dict(self):
        data = {
            'id': self.id,
            'author_id': self.author_id,
            'created': self.created.isoformat() + 'Z',
            'edited': self.edited.isoformat() + 'Z',
            'placeholder': self.placeholder,
            'substitute': self.substitute,
            '_links': {
                'self': url_for('api.get_replacement', id=self.id),
                'user': url_for('api.get_replacement_author', id=self.id)
            }
        }
        return data

    def from_dict(self, data):
        for field in ['placeholder', 'substitute', 'author_id']:
            if field in data:
                setattr(self, field, data[field])

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Permission:
    DICTATE = 1
    MODERATE = 2
    ADMIN = 4


def getDialects():
    path = os.path.join(
        os.path.dirname(__file__), "static/js/webspeech/langs.js")  
    data = open(path, "r").read()
    langStr = data.split("/* START */ `")[1].split("`/* END */")[0]

    langJson = json.loads(langStr)
    
    langs = []
    for line in langJson:
        for e in line[1:]:
            langs.append(e[0])

    return tuple(langs)
dialects = getDialects()
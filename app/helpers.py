def unsalt(hash):
    '''Removes the meta information from a password hash, to make it easier to send it in an http header'''
    return hash.split(':')[-1]
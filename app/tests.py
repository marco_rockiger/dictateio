"""JavaScript Tests"""
from flask import render_template, Blueprint

tests = Blueprint('tests', __name__)

@tests.route('/tests')
def index():
    return render_template('tests/tests.html')
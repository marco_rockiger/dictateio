from flask import Blueprint
from app.models import Permission

pages = Blueprint('pages', __name__)

from . import views, errors

@pages.app_context_processor
def inject_permission():
    return dict(Permission=Permission)
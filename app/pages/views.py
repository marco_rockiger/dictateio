from flask import render_template, g, redirect, url_for

from . import pages

@pages.route('/')
def index():
    if g.user:
        return redirect(url_for('docs.mydocs'))
    return render_template('pages/index.html')


@pages.route('/help')
def help():
    return render_template('pages/help.html')


@pages.route('/signup')
def signup():
    return render_template('auth/signupbeta.html')

@pages.route('/privacy')
def privacy():
    return render_template('pages/privacy.html')

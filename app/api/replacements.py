from datetime import datetime

from flask import jsonify, request, url_for

from app.api.auth import token_auth
from app import db
from app.models import Replacement, User
from app.api import bp
from app.api.errors import bad_request

@bp.route('/replacements/<int:id>', methods=['GET'])
@token_auth.login_required
def get_replacement(id):
    return jsonify(Replacement.query.get_or_404(id).to_dict())

@bp.route('/replacements', methods=['GET'])
@token_auth.login_required
def get_replacements():
    page = request.args.get('page', 1, type=1)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Replacement.to_collection_dict(Replacement.query, page, per_page, 'api.get_replacements')
    return jsonify(data)

@bp.route('/replacements', methods=['POST'])
@token_auth.login_required
def create_replacment():
    data = request.get_json() or {}
    if 'placeholder' not in data or 'substitute' not in data or 'author_id' not in data:
        return bad_request('must include placeholder, substitute and author_id fields')
    replacement = Replacement()
    replacement.from_dict(data)
    db.session.add(replacement)
    db.session.commit()
    response = jsonify(replacement.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_replacement', id=replacement.id)
    return response

@bp.route('/replacements/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_replacement(id):
    data = request.get_json() or {}
    if 'placeholder' not in data or 'substitute' not in data or 'author_id' not in data:
        return bad_request('must include placeholder, substitute and author_id fields')
    replacement = Replacement.query.get_or_404(id)
    replacement.from_dict(data)
    replacement.edited = datetime.utcnow()

    db.session.commit()
    response = jsonify(replacement.to_dict())
    return response

@bp.route('/replacements/<int:id>/author', methods=['GET'])
@token_auth.login_required
def get_replacement_author(id):
    replacement = Replacement.query.get_or_404(id)
    return jsonify(replacement.author_id)

@bp.route('/replacements/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_replacement(id):
    replacement = Replacement.query.filter_by(id=id).first()
    db.session.delete(replacement)
    db.session.commit()
    return '', 204
from datetime import datetime

from flask import jsonify, request, url_for

from app.api.auth import token_auth
from app import db
from app.models import Doc, User
from app.api import bp
from app.api.errors import bad_request

@bp.route('/docs/<int:id>', methods=['GET'])
@token_auth.login_required
def get_doc(id):
    return jsonify(Doc.query.get_or_404(id).to_dict())

@bp.route('/docs', methods=['GET'])
@token_auth.login_required
def get_docs():
    page = request.args.get('page', 1, type=1)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Doc.to_collection_dict(Doc.query, page, per_page, 'api.get_docs')
    return jsonify(data)

@bp.route('/docs', methods=['POST'])
@token_auth.login_required
def create_doc():
    data = request.get_json() or {}
    if 'title' not in data or 'body' not in data or 'author_id' not in data:
        return bad_request('must include title, body and author_id fields')
    doc = Doc()
    doc.from_dict(data)
    db.session.add(doc)
    db.session.commit()
    response = jsonify(doc.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_doc', id=doc.id)
    return response

@bp.route('/docs/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_doc(id):
    data = request.get_json() or {}
    if 'title' not in data or 'body' not in data or 'author_id' not in data:
        return bad_request('must include title, body and author_id fields')
    doc = Doc.query.get_or_404(id)
    doc.from_dict(data)
    doc.edited = datetime.utcnow()

    db.session.commit()
    response = jsonify(doc.to_dict())
    return response

@bp.route('/docs/<int:id>/author', methods=['GET'])
@token_auth.login_required
def get_author(id):
    doc = Doc.query.get_or_404(id)
    return jsonify(doc.author_id)
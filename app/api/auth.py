from flask import g
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth

from werkzeug.security import check_password_hash

from app.models import User
from app.api.errors import error_response

basic_auth = HTTPBasicAuth() # for password verification of user
token_auth = HTTPTokenAuth() # for token verification of user

@basic_auth.verify_password
def verify_password(email, password):
    user = User.query.filter_by(email=email).first()
    if user is None:
        return False
    g.current_user = user
    # user part of password hash without salt information
    return user.password.split(':')[-1] == password

@basic_auth.error_handler
def basic_auth_error():
    return error_response(401)


@token_auth.verify_token
def verify_token(token):
    g.current_user = User.check_token(token) if token else None
    return g.current_user is not None

@token_auth.error_handler
def token_auth_error():
    return error_response(401)
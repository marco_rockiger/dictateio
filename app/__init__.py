import os

from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_pure import Pure
from flask_login import LoginManager
from flask_sslify import SSLify
from flask_admin import Admin
from flask_mail import Mail

from app.admin import AdminModelView, AdminUserModelView, AdminDocModelView

#! Initialize the db extension, but without congiguring
#! it with an application instance.
db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
mail = Mail()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(app.instance_path, 'app.sqlite'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False
    )
    

    if test_config is None:
        # load the instance config, if it exists, when not test
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    if app.config.get("SSLIFY"):
        sslify = SSLify(app, permanent=True)

    # Initialize any extensions and bind blueprint to the
    # application instance here
    db.init_app(app)
    migrate.init_app(app, db)
    Pure(app)
    login_manager.init_app(app)
    mail.init_app(app)

    if app.config.get("ADMIN_URL"):
        admin = Admin(app, url='/' + app.config.get("ADMIN_URL"))
        admin.add_view(AdminUserModelView(models.User, db.session))
        admin.add_view(AdminModelView(models.Role, db.session))
        admin.add_view(AdminDocModelView(models.Doc, db.session))
        admin.add_view(AdminModelView(models.Replacement, db.session))
        
    

    from . import auth
    app.register_blueprint(auth.auth)

    from . import docs
    app.register_blueprint(docs.docs)

    from . import pages
    app.register_blueprint(pages.pages)
    app.add_url_rule('/', endpoint='index')

    from . import tests
    app.register_blueprint(tests.tests)

    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    return app

# import models to be available for flask_migrate
# the import is at the bottom, to prevent circular
# imports
from app import models
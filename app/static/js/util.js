/**
 * convinience function for document.querySelector and document.querySeletorAll
 * @param {String} query - the css query
 * @returns {(Element|NodeList)}
 */
export function _(query) {
	if (query.indexOf("#") !== -1) {
		return document.querySelector(query);
	} else {
		return document.querySelectorAll(query);
	}
}


/**
 * Returns CSS rem in pixels
 * @param {number} rems - the number of rems
 * @return {number} 
 */
export function rem(rems) {    
    return rems * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

/**
 * returns x, y coordinates for absolute positioing of a span within a given text input
 * or textarea at a given selection point
 * @param {Element} input - input element to optain coordinates for
 * @param {Number} selectionPoint - the selection point for the input
 * @returns {object} - with x and y members
 */
export function getCursorXY(input, selectionPoint) {
	const {
		offsetLeft: inputX,
		offsetTop: inputY
	} = input

	// create a dummy element tha will be aclone of our input
	const div = document.createElement('div');
	// get the computetd style of the input and clone it onto the dummy elment
	const copyStyle = getComputedStyle(input);
	for (const prop of copyStyle) {
		div.style[prop] = copyStyle[prop];
	}
	// we need a character that will replace whitespace when filling our dummy element if 
	// it's a single line <input/>
	const swap = '.';
	const inputValue = input.tagName === 'INPUT' 
					   ? input.nodeValue.replace(/ /g, swap) 
					   : input.value;
	// set the div content to that of the textarea up until selection
	const textContent = inputValue.substr(0, selectionPoint);
	// set the text content of the dymmy element div
	div.textContent = textContent;
	if (input.tagName === 'TEXTAREA') {
		div.style.heigh = 'auto';
	}
	// if a single line input then the div needs to be single line and not break out like 
	// the text area
	if (input.tagName === 'INPUT') {
		div.style.width = 'auto';
	}
	// create a marker element to obtain caret position
	const span = document.createElement('span');
	// give the span the textContent of remaining content so that the recreated dummd element
	// is a close as possible
	span.textContent = inputValue.substring(selectionPoint) || '.'
	// append the span marker to the div
	div.appendChild(span);
	// append the dummy element to the body
	document.body.appendChild(div);
	//get the marker position, this is the caren position top and left relative to the input
	const {offsetLeft: spanX, offsetTop: spanY} = span;
	//lastly, remove that dummy element
	// NOTE:  can comment this out for debbugging purposes, if you want to see wher that
	// span is rendered
	document.body.removeChild(div);
	// return an object with x and y of the caret. account for input positioning so that
	// you don't need to wrap the input
	return {
		x: inputX + spanX,
		y: inputY + spanY
	}
}

/**
 * returns y coordinate for absolute positioing of a span within a given text input
 * or textarea at a given selection point
 * @param {Element} input - input element to optain coordinates for
 * @param {Number} selectionPoint - the selection point for the input
 * @returns {number}
 */
export function getCursorY(input, selectionPoint) {
	return getCursorXY(input, selectionPoint).y
}

/**
 * checks if the cursor is in the current view
 * @param {number} ypos -  the y position of the cursor
 * @param {Element} el - the scrollable element
 * @param {number} height - the height of the element
 * @param {number} padding - the padding of a view, default is 80
 * @returns {boolean}
 */
export function isCursorInView(ypos, el, height, padding=16) {
	
	const upperBoundary = el.scrollTop + padding;
	const lowerBoundary = el.scrollTop + height - padding;
	console.log('upperBoundary: ' + upperBoundary + '    lowerBoundary: ' + lowerBoundary + '   ypos: ' + ypos + '\n');
	
	if (ypos >= upperBoundary && ypos <= lowerBoundary) {
		return true;
	}
	return false;
}


/**
 * Resizes the given element to the size of its content 
 * 
 * @param {Element} el - the DOM element to resize usually a textarea
 */
export function resize (el) {
	setTimeout(() => {
		el.style.height = el.scrollHeight + 'px';
	}, 0);
}

export function isEmpty(arr) {
	return arr.length === 0;
}

export function first(arr) {
	return arr[0];
}

export function rest(arr) {
	return arr.slice(1);
}

export function last(arr) {
	return arr[arr.length - 1];
}
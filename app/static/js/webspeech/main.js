import { DOCFORM, BODY, EDITOR } from './constants.js';
import { setupLanguage } from './speechRecognition.js';
import { setupAutomaticSubstitution } from './substitution.js';
import { setupAutosave, saveDoc, revokeToken } from './requests.js';
import { setupTinyMCE } from './tinymce.js';
import { setupPWA } from './beyondgrammar-tinymce/beyond-grammar-plugin.js';
import { setupTmceDictandu } from './tinymce-dictandu-plugin/tinymce-dictandu.plugin.js'; 

// Setup the View
setupPage();
setupLanguage();
setupPWA();
setupTmceDictandu();
setupTinyMCE(BODY.value, BODY.placeholder);
setupAutosave();
setupAutomaticSubstitution();

/*******************
 * Async Functions *
 *******************/
function setupPage() {
    DOCFORM.addEventListener('submit', submitDocform);
    window.addEventListener('beforeunload', onBeforeunload);
    window.addEventListener('close', onBeforeunload);
}

/**
 * Move the editor content to the textarea befor submitting the form
 * @param {object ev - the submit event}
 */
export async function submitDocform(ev) {
    /* const editorContent = PMMD.defaultMarkdownSerializer.serialize(
        EDITOR.state.doc,
    );
    BODY.value = editorContent;
    window.location.href = '/'; */
    await saveDoc();
    ev.preventDefault();
}

export async function onBeforeunload() {
    await saveDoc();
    await revokeToken();
}

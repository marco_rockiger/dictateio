import { state } from '../state.js';
import { _ } from '../../util.js';


const SEARCH_FORM_HTML = `<div id="searchform" class="ui mini form hidden" style="display: none;">
        <div class="inline fields">
          <label>Find</label>
          <div class="field">
            <input id="searchform-find-input" type="text" placeholder="Find...">
          </div>
          <label>Replace</label>
          <div class="field">
            <input id="searchform-replace-input" type="text" placeholder="Replace...">
          </div>
          <div class="field">
              <div id="searchform-find-button" class="ui submit mini button">Find</div>
          </div>
          <div class="field">
            <div id="searchform-replace-button" class="ui submit mini button">Replace</div>
          </div>
          <div class="field">
            <div id="searchform-all-button" class="ui submit mini button">Replace All</div>
          </div>
        </div>
      </div>`;


let $searchTerm$ = '';
let $searchPos$ = 1;
let $numberOfResults$ = '';
let searchForm;

export function setupTmceDictandu() {
  tinymce.PluginManager.add('findreplace', function (editor, url) {
    // Add a button that opens a window
    editor.addButton('findreplace', {
      text: '',
      icon: 'search',
      stateSelector: 'stateSelector',
      onclick: toggleSearch(),
    });
    editor.shortcuts.remove("Meta+F");
    editor.shortcuts.remove('Ctrl+f');
    editor.shortcuts.add("Ctrl+Shift+f", "Open Search", 
                         () => document.getElementById('mceu_11-button').click());

    let alreadyLoaded = false;
    editor.on('init', () => {
      if (alreadyLoaded) {
        //protecting from word press twice init. https://tommcfarlin.com/wordpress-hooks-firing-twice/
        return;
      }
      alreadyLoaded = true;
      searchForm = $(SEARCH_FORM_HTML)[0];
      $(searchForm).insertAfter('#interim_wrapper');

      _('#searchform-find-button').addEventListener('click', ev => {
        search();
      });
      _('#searchform-replace-button').addEventListener('click', ev => {
        replace();
      });
      _('#searchform-all-button').addEventListener('click', ev => {
        tinymce.activeEditor.plugins.searchreplace.replace(_('#searchform-replace-input').value, true, true);
        $numberOfResults$ = 0;
      });
      _('#searchform-replace-input').addEventListener('keydown', ev => {
        if (ev.key === 'Enter') {
          ev.preventDefault();
          replace()
        }
      })
      _('#searchform-find-input').addEventListener('keydown', ev => {
        if (ev.key === 'Enter') {
          ev.preventDefault();
          search();
        }
      })
      
      console.log(searchForm);
    });
    return {
      getMetadata: function () {
        return {
          name: "Example plugin",
          url: "http://exampleplugindocsurl.com"
        };
      }
    };
  });
}


function replace() {
  tinymce.activeEditor.plugins.searchreplace.replace(_('#searchform-replace-input').value);
  $numberOfResults$ = $numberOfResults$ - 1;
}

function search() {
  let newSearchTerm = _('#searchform-find-input').value;
  if (newSearchTerm !== $searchTerm$) {
    $searchTerm$ = newSearchTerm;
    $numberOfResults$ = tinymce.activeEditor.plugins.searchreplace.find($searchTerm$);
  }
  else {
    if ($searchPos$ < $numberOfResults$) {
      tinymce.activeEditor.plugins.searchreplace.next();
      $searchPos$++;
    }
    else {
      $numberOfResults$ = tinymce.activeEditor.plugins.searchreplace.find($searchTerm$);
      $searchPos$ = 1;
    }
  }
}

function toggleSearch() {
  return function () {
    console.log('State:', state);
    if (!state.$recognizing$ && !state.$search$) {
      console.log(this.active());
      openSearch(this, searchForm);
    }
    else {
      closeSearch(this, searchForm);
    }
  };
}

export function closeSearch(button, form) {
  form.style.display = 'none';
  tinymce.activeEditor.plugins.searchreplace.done(false);
  button.active(false);
  state.$search$ = false;
  $searchTerm$ = '';
  $searchPos$ = 0;
  _('#searchform-replace-input').value = '';
  _('#searchform-find-input').value = '';
}

export function openSearch(button, form) {
  form.style.display = 'block';
  button.active(true);
  _('#searchform-find-input').focus();
  state.$search$ = true;
}
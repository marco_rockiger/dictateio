import { _, last } from '../util.js';
// import { setupEditor } from './editor.js';
import { setupTinyMCE } from './tinymce.js'
import { setupSpeechRecognition } from './speechRecognition.js';

/*************
 * Constants *
 *************/
export const TITLE = _('#title');
export const BODY = _('#body');
export const START_BUTTON = _('#start_button');
export const SELECT_LANGUAGE = _('#select_language');
export const SELECT_DIALECT = _('#select_dialect');
export const SETTINGS_BUTTON = _('#settings_button');
export const INFO_BUTTON = _('#info_button');
export const EXPORT_BUTTON = _('#export_button');
export const REVIEW_HELP = _('#reviewhelp');
export const DICTATE_HELP = _('#dictatehelp');
export const DOCFORM = _('#docform');
export const INTERIM_SPAN = _('#interim_span');
export const INTERIM_WRAPPER = _('#interim_wrapper');
export const FOLDERTOKENS = 'api/tokens';
export const FOLDERDOCS = 'api/docs';
export const REPLACEMENTSDOCS = 'api/replacements';
// export const PM = window.PM;
// export const PMView = PM.view;
// export const PMState = PM.state;
// export const PMExampleSetup = PM.example_setup;
// export const PMMD = window.PMMD;
export const MARKED = window.marked;
//export const EDITOR = setupEditor(BODY.value, BODY.placeholder);
// export const PWA = last(EDITOR.state.plugins);
export const EDITOR = window.tinymce;
export const $ = window.$;
export const RECOGNITION = setupSpeechRecognition();

export const REPLACEMENTS = [
    [' period', '.'],
    [' comma', ','],
    [' Exclamation Point', '!'],
    [' question mark', '?'],
    [' new line ', '<br />'],
    [' new paragraph ', '</p><p>'],
    [' colon', ':'],
    [' semicolon', ';'],
    ['\n\n', '</p><p>'],
    [' punkt ', '. '],
    [' punkt',  '.'],
    [' komma ', ', '],
    [' komma', ','],
    ['komma', ','],
    [' Ausrufezeichen', '!'],
    ['Ausrufezeichen', '!'],
    [' Fragezeichen', '?'],
    ['Fragezeichen', '?'],
    [' neue Zeile ', '<br />'],
    [' absatz ', '</p><p>'],
    [' Doppelpunkt', ':'],
    ['Doppelpunkt', ':'],
    [' Semikolon', ';'],
    ['Semikolon', ';'],
];

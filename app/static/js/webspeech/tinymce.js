import { MARKED, EDITOR, EXPORT_BUTTON, TITLE, SELECT_DIALECT } from './constants.js';
import { _ } from '../util.js';
import { onClickStartButton } from './speechRecognition.js';

import { state } from './state.js';
import { improve_final_transcript } from './string.js';
import { deactivatePwa, activatePwa } from './beyondgrammar-tinymce/beyond-grammar-plugin.js';

const CONTENT_STYLE = `
    body {
        font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif !important; 
        font-size: 18px !important;
        padding: 1rem 10vw 50vh;
    }
    @media only screen and (min-width: 769px) {
        body { padding: 2rem 20vw 50vh; }
    }
    ::-webkit-scrollbar {
        -webkit-appearance: none;
        width: 10px;
        height: 10px;
    }    
    ::-webkit-scrollbar-thumb {
        cursor: pointer;
        border-radius: 5px;
        background: rgba(0,0,0,.25);
        -webkit-transition: color .2s ease;
        transition: color .2s ease;
    }
    ::-webkit-scrollbar-track {
        background: rgba(0,0,0,.1);
        border-radius: 0;
    }
    ::selection {
        background-color: #cce2ff;
        color: rgba(0,0,0,.87);
    }`;

export async function setupTinyMCE(content, placeholder) {
    const tinymce = EDITOR;
    const editors = await tinymce.init({
        selector: '#editor',
        themes: 'modern',
        plugins: 'placeholder BeyondGrammar searchreplace wordcount findreplace',
        placeholder: placeholder,
        menubar: false,
        content_style: CONTENT_STYLE,

        // Prowritingaid options
        toolbar: [
            "bold italic | undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | findreplace"
          ],
          entity_encoding: 'raw',
          entities: '',

          init_instance_callback: function (editor) {
            // needs to be defined, because the shortcut from the host page doesn't come 
            // through. The cause is that tinymce is in a iFrame
            editor.shortcuts.add("alt+s", "Start dictation", onClickStartButton);
          },

        //all options placed in `bgOptions` object
  bgOptions: {
    service: {

      //localization for BeyondGrammar interface, will be used default or got by system information, please use same version with the plugin.
      i18n: {
        en: "https://prowriting.azureedge.net/beyondgrammar-tinymce/1.0.16/dist/i18n-en.js"
      },

      //You should signup for getting this key
      apiKey: "0AAB4A28-47F4-43F2-A474-7F6B172810C7",

      //[optional] You can specify it for permanent access to your settings and dictionaries
      //userId: "<YOUR_USER_ID>",

      //[optional] path to js file with BeyondGrammar Core
      //sourcePath : "", 

      //[optional] path to service which provides grammar checking
      // url shouldn's contains / in the end
      //serviceUrl : "https://rtg.prowritingaid.com/api/v1", 
    },
    grammar: {
      //[optional] Default language [en-US, en-GB],
      languageIsoCode: "en-US",

      //[optional] checking Style. By default is "true"
      checkStyle: true,

      //[optional] checking Spelling. By default is "true"
      checkSpelling: true,

      //[optional] checking Grammar. By default is "true"
      checkGrammar: true,

      //[optional] Show thesaurus information by double click, by default true
      showThesaurusByDoubleClick: true,

      //[optional] Showing context thesaurus, works only if showThesaurusByDoubleClick = true, by default false
      showContextThesaurus: false,
    }
  }
    });
    const editor = editors[0];

    // MARKED.setOptions({
    //     sanitize: true  // can't use sanitize because it replaces brackets
    // });
    editor.insertContent(MARKED(content));


    EXPORT_BUTTON.addEventListener('click', onClickExportButton);
    document.addEventListener('keyup', onKeyUp);

  return tinymce;
}

/**
 * 
 * @param {tinymce.dom.Selection} selection 
 * @param {int} offset - the offset to the left
 * @returns {char} - the character at the offset position
 */
function getCharBefore(selection, offset) {
    const orgRng = selection.getRng().cloneRange();
    const newRng = selection.getRng().cloneRange();
    const newStartOffset = newRng.startOffset - offset;
    const newEndOffset = newRng.startOffset - offset + 1;
    try {
        newRng.setStart(newRng.startContainer, newStartOffset);
        newRng.setEnd(newRng.startContainer, newEndOffset);
        selection.setRng(newRng);
        const charBeforeOffset = selection.getContent({format: 'raw'});
        selection.setRng(orgRng);
        return charBeforeOffset;
    } catch (error) {
        console.log('throw');
        throw error
    } finally {
        console.log('finally');
        selection.setRng(orgRng);
    }
}

/**
 * Checks if the a space should be inserted into the editor
 * @param {tinymce.dom.Selection} selection
 * @returns {boolean}
 */
export function needsSpace(selection) {
    let char1BeforeOffset = '';
    let char2BeforeOffset = '';
    try {
        char1BeforeOffset = getCharBefore(selection, 1)
    } catch (error) {
        return false;        
    }

    try {
        char2BeforeOffset = getCharBefore(selection, 2)
    } catch (error) {
        // Do nothing right now
    }

    if (char1BeforeOffset !== ' ' && char1BeforeOffset !== '\xa0') return true;

    return false;
}
window.needsSpace = needsSpace;

/**
 * checks if the next character at the current selection should be capitilized
 * @param {prosemirror-state} state the state where the current selection belongs
 */
export function isCapital(selection) {
    // TODO
    // debugger;
    let char1BeforeOffset = '';
    let char2BeforeOffset = '';
    try {
        char1BeforeOffset = getCharBefore(selection, 1)
    } catch (error) {
        return true;        
    }

    try {
        char2BeforeOffset = getCharBefore(selection, 2)
    } catch (error) {
        // Do nothing right now
    }

    if (
        (['.', '!', '?'].includes(char2BeforeOffset) &&
            (char1BeforeOffset === ' ' || char1BeforeOffset === '\xa0')) ||
        ['.', '!', '?'].includes(char1BeforeOffset)) return true;

    return false;
}
window.isCapital = isCapital;

/**
 * Change the visibility of the export button and it's popup
 * @param {object} ev - The click event
 */
export function onClickExportButton() {
    const body = EDITOR.activeEditor.getContent();
    const title = TITLE.value.length > 0 ? '<h1>' + TITLE.value + '</h1>\n' : '';
    const popup = $('#export_popup').modal({
        centered: false
    });
    popup.modal('show');
    const ta = _('#export_popup textarea');
    //const menu = _('.ProseMirror-menubar')[0];

    MARKED.setOptions({
        sanitize: true
    });
    ta.value = title + body;

    //menu.classList.toggle('zIndex0');
    ta.select();
}

export function onKeyUp(ev) {
    const isMac = navigator.platform.indexOf('Mac') !== -1 ? true : false;
    const ky = ev.keyCode;
    const key = ev.key;
    const ctrlPressed = isMac ? ev.metaKey : ev.ctrlKey;
    const altPressed = isMac ? ev.ctrlKey : ev.altKey;
    const shiftPressed = ev.shiftKey;

    if (state.$recognizing$) {
        // not necessary at the moment
        // let isMac = ((process.platform === "darwin") ? true : false);

        switch (key) {
        case '.':
        case 'ArrowUp': //Up, because of presenter
            periodButton();
            ev.preventDefault();
            break;

        case ',':
        case 'ArrowDown': // down, on presenter
            commaButton();
            ev.preventDefault();
            break;

        case 'Enter': // the same on presenter
            paragraphButton();
            ev.preventDefault();
            break;

        case ':': // there is no Colon button
        case 'Tab': // tab on presenter
            colonButton();
            ev.preventDefault();
            break;

        case '?':
            console.log('Questionmark');
            // TODO questionButton();
            break;

        case '!':
            console.log('Exclamationmark');
            // TODO exclamationButton();
            break;

        case ';':
            console.log('semicolon');
            // TODO semicolonButton();
            break;

        default:
            break;
        }
    }

    if (altPressed) {
        switch (key) {
            case 's':
                onClickStartButton(ev);
                ev.preventDefault();
                break;
            case 'f':
                document.getElementById('mceu_11-button').click();
                ev.preventDefault();
                break;
        
            default:
                break;
        }
    }

    if (ctrlPressed && shiftPressed) {
        console.log('ctrlPressed && shiftPressed');
        console.log('key:', key);
        switch (key) {
            case 'F':
                console.log('ctrl+f');
                document.getElementById('mceu_11-button').click();
                ev.preventDefault();
                break;
        
            default:
                break;
        }
    }
    // reactivate if you develop new shortcuts
    // console.log(ky);
    // console.log(key);
}

    export function improveBeforEditing() {
        let content = tinymce.activeEditor.getContent();
        content = improve_final_transcript(content);
        tinymce.activeEditor.setContent('');
        tinymce.activeEditor.insertContent(content);
    }


/***********
 * Helpers *
 ***********/
/**
 * Enable the textarea of the editors textarea
 */
export function enableBody() {
    //EDITOR.activeEditor.setMode('design');
    if (SELECT_DIALECT.value.startsWith('en-')) activatePwa();
}

/**
 * Disable the textarea of the editors textarea
 */
export function disableBody() {
    //EDITOR.activeEditor.setMode('readonly');
    if (SELECT_DIALECT.value.startsWith('en-')) deactivatePwa();
}
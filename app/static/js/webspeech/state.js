/*********
 * State *
 *********/
export const state = {
    $recognizing$: false,
    $restart$: true,
    $ignore_onend$: true,
    $start_timestamp$: null,
    // to know if a punctuation has to be added
    $punctuation$: '',
    $capitalize$: false,
    // for autosaving
    $email$: null,
    $hash$: null,
    $token$: null,
    $docId$: null,
    $authorId$: null,
    $savedTitle$: null,
    $savedBody$: null,
    $savedDialect$: null,
    $replacements$: [],
    $continuous$: true,
    $interimResults$: true,
    $search$: false,
};

// check for Android 
if (navigator.userAgent.match(/Android/i)) {
    state.$continuous$ = false;
    state.$interimResults$ = false;
    state.$restart$ = true;
    state.$ignore_onend$ = true;
}
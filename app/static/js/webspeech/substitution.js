import { $, REPLACEMENTSDOCS } from './constants.js';

import { state } from './state.js';
import { getToken } from './requests.js';

export function setupAutomaticSubstitution() {
    const automaticSubstitionRow = $('#automatic-substitution-row');

    $('#placeholder').on('input', onInputSubstitution);
    $('#substitution').on('input', onInputSubstitution);

    // eslint-disable-next-line quotes
    let substitutions = $('#replacements').data('replacements');

    // if there are no substituions the type of $().type is object and not string
    if (typeof substitutions === 'string') {
        try {
            substitutions = substitutions.replace(/'/gm, '"');
            substitutions = JSON.parse(substitutions);
        } catch (e) {
            automaticSubstitionRow.after(`<div class="ui info message">
            <i class="close icon"></i>
            <div class="header">
            Oops, something went wrong.
            </div>
            We couldn't load your substitutions.
        </div>`);
            $('.message .close').on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade');
            });
            console.warn(e);
        }
        for (const subs of substitutions) {
            automaticSubstitionRow.after(
                createMySubstitutionRow(subs[1], subs[2], subs[0])
            );
        }
    }
}

export function createMySubstitutionRow(
    placeholderValue,
    substitutionValue,
    replacementId = null
) {
    // Start Crate substituion row
    const mySubstitutionRow = document.createElement('div');
    mySubstitutionRow.classList = 'ui form my-substitution__row';
    if (replacementId) mySubstitutionRow.dataset.id = replacementId;

    const inlineFields = document.createElement('div');
    inlineFields.classList = 'inline fields';
    mySubstitutionRow.appendChild(inlineFields);

    const oneWideField1 = document.createElement('div');
    oneWideField1.classList = 'one wide field';
    oneWideField1.style = 'width: 1.75rem !important;';
    inlineFields.appendChild(oneWideField1);

    const uiCheckbox = document.createElement('div');
    uiCheckbox.classList = 'ui checked checkbox';
    oneWideField1.appendChild(uiCheckbox);

    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.setAttribute('checked', true);
    uiCheckbox.appendChild(checkbox);

    const label = document.createElement('label');
    uiCheckbox.appendChild(label);

    const sevenWideField1 = document.createElement('div');
    sevenWideField1.classList = 'seven wide field';
    inlineFields.appendChild(sevenWideField1);

    const myPlaceholder = document.createElement('input');
    myPlaceholder.setAttribute('type', 'text');
    myPlaceholder.classList = 'my-placeholder';
    myPlaceholder.setAttribute('value', placeholderValue);
    sevenWideField1.appendChild(myPlaceholder);

    const sevenWideField2 = document.createElement('div');
    sevenWideField2.classList = 'seven wide field';
    inlineFields.appendChild(sevenWideField2);

    const mySubstitution = document.createElement('input');
    mySubstitution.setAttribute('type', 'text');
    mySubstitution.classList = 'my-substitution';
    mySubstitution.setAttribute('value', substitutionValue);
    sevenWideField2.appendChild(mySubstitution);

    const oneWideField2 = document.createElement('div');
    oneWideField2.classList = 'one wide field';
    inlineFields.appendChild(oneWideField2);

    const closeLinkIcon = document.createElement('i');
    closeLinkIcon.classList = 'close link icon';
    oneWideField2.appendChild(closeLinkIcon);
    // End Create substituion row

    closeLinkIcon.addEventListener('click', () => {
        mySubstitutionRow.remove();
        deleteReplacement(mySubstitutionRow.dataset.id);
    });
    // eslint-disable-next-line no-inner-declarations
    function save() {
        console.log('Save Replacement');
        saveReplacement(
            myPlaceholder.value,
            mySubstitution.value,
            mySubstitutionRow
        );
    }
    myPlaceholder.addEventListener('change', save);
    mySubstitution.addEventListener('change', save);
    return mySubstitutionRow;
}

export function onInputSubstitution(ev) {
    const placeholder = $('#placeholder');
    const substitution = $('#substitution');
    const placeholderValue = placeholder.val();
    const substitutionValue = substitution.val();

    if (placeholderValue && substitutionValue) {
        const automaticSubstitionRow = $('#automatic-substitution-row');
        const targetID = ev.target.id;
        const mySubstitutionRow = createMySubstitutionRow(
            placeholderValue,
            substitutionValue
        );

        console.log(mySubstitutionRow);
        automaticSubstitionRow.after(mySubstitutionRow);
        const focusInput = $(`.my-${targetID}`).first();
        focusInput.focus();
        const tmp = focusInput.val();
        focusInput.val('');
        focusInput.val(tmp);

        placeholder.val('');
        substitution.val('');
    }
}

async function saveReplacement(placeholder, replacement, substitutionRow) {
    if (!state.$token$) {
        await getToken(state.$email$, state.$hash$);
        await saveReplacement(placeholder, replacement);
    } else {
        const requestData = {
            placeholder: placeholder,
            substitute: replacement,
            author_id: state.$authorId$
        };
        console.log(JSON.stringify(requestData));

        let url = '';
        let method = '';
        if (substitutionRow.dataset.id) {
            url = new URL(
                REPLACEMENTSDOCS + '/' + substitutionRow.dataset.id,
                window.location.origin
            );
            method = 'PUT';
        } else if (placeholder || replacement) {
            //new non-empty document
            url = new URL(REPLACEMENTSDOCS, window.location.origin);
            method = 'POST';
        }

        const request = new Request(url, {
            method: method,
            url: url,
            headers: {
                Authorization: `Bearer ${state.$token$}`,
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(requestData)
        });

        try {
            const response = await fetch(request);
            const responseData = await response.json();
            console.log(response);
            console.log(responseData);
            const replacementId = responseData.id;
            substitutionRow.dataset.id = replacementId;
            if (response.status === 401) {
                await getToken(state.$email$, state.$hash$);
                await saveReplacement();
            } else if (response.status >= 200 && response.status < 300) {
                console.log('Replacement');
            }
        } catch (error) {
            // console.log(`Error during ${method} of document:`);
            // console.log(error);
        }
    }
}

async function deleteReplacement(replacementId) {
    if (!replacementId) return;
    if (!state.$token$) {
        await getToken(state.$email$, state.$hash$);
        await deleteReplacement(replacementId);
    } else {
        const requestData = {
            id: replacementId
        };
        console.log(JSON.stringify(requestData));

        let url = '';
        let method = 'DELETE';
        url = new URL(
            REPLACEMENTSDOCS + '/' + replacementId,
            window.location.origin
        );
        const request = new Request(url, {
            method: method,
            url: url,
            headers: {
                Authorization: `Bearer ${state.$token$}`,
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=utf-8'
            }
        });

        try {
            const response = await fetch(request);
            const responseData = await response.json();
            console.log(response);
            console.log(responseData);
            if (response.status === 401) {
                await getToken(state.$email$, state.$hash$);
                await deleteReplacement(replacementId);
            } else if (response.status >= 200 && response.status < 300) {
                console.log('Delete Replacement');
            }
        } catch (error) {
            // console.log(`Error during ${method} of document:`);
            // console.log(error);
        }
    }
}

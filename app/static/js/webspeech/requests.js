import { 
    SELECT_DIALECT, 
    TITLE, 
    FOLDERDOCS, 
    EDITOR, 
    FOLDERTOKENS,
    BODY, 
    MARKED,
} from './constants.js';

import { state } from './state.js';
import { _ } from '../util.js';

export async function setupAutosave() {
    const { email, hash, docId, authorId } = getCredentials();

    state.$email$ = email;
    state.$hash$ = hash;
    state.$docId$ = docId;
    state.$authorId$ = authorId;
    state.$savedTitle$ = TITLE.value;
    state.$savedBody$ = MARKED(BODY.value);
    state.$savedDialect$ = SELECT_DIALECT.value;

    await getToken(state.$email$, state.$hash$);

    setInterval(saveDoc, 30000);
}

export async function saveDoc() {
    if (!state.$token$) {
        await getToken(state.$email$, state.$hash$);
        await saveDoc();
    }
    else {
        // get title, body and language
        const title = TITLE.value;
        const body = EDITOR.activeEditor.getContent();
        const dialect = SELECT_DIALECT.value;
        if (hasContentChanged(title, body, dialect)) {
            console.log('No need to save, data hasn\'t changed');
            return;
        }
        const requestData = {
            title: title,
            body: body,
            author_id: state.$authorId$,
            dialect: dialect,
        };
        // console.log(JSON.stringify(requestData));
        let url = '';
        let method = '';
        if (state.$docId$) {
            url = new URL(FOLDERDOCS + '/' + state.$docId$, window.location.origin);
            method = 'PUT';
        }
        else if (title || body) {
            //new non-empty document
            url = new URL(FOLDERDOCS, window.location.origin);
            method = 'POST';
        }
        const request = new Request(url, {
            method: method,
            url: url,
            headers: {
                Authorization: `Bearer ${state.$token$}`,
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify(requestData),
        });
        try {
            const response = await fetch(request);
            const responseData = await response.json();
            // console.log(response);
            // console.log(responseData);
            if (response.status === 401) {
                await getToken(state.$email$, state.$hash$);
                await saveDoc();
            }
            else if (response.status >= 200 && response.status < 300) {
                state.$docId$ = responseData.id;
                state.$savedTitle$ = title;
                state.$savedBody$ = body;
                state.$savedDialect$ = dialect;
            }
        }
        catch (error) {
            // console.log(`Error during ${method} of document:`);
            // console.log(error);
        }
    }
}

export async function getToken(email, hash) {
    const url = new URL(FOLDERTOKENS, window.location.origin);
    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                Authorization: `Basic ${btoa(email + ':' + hash)}`,
            },
        });
        const data = await response.json();
        if ('token' in data) state.$token$ = data['token'];
        // console.log($token$);
    } catch (error) {
        // console.log('could not load token');
        // console.log(error);
        state.$token$ = null;
    }
}

export async function revokeToken() {
    const url = new URL(FOLDERTOKENS, window.location.origin);
    try {
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${state.$token$}`,
            },
        });
        const data = await response.json();
        state.$token$ = null;
        // console.log($token$);
    } catch (error) {
        // console.log('could not load token');
        // console.log(error);
        state.$token$ = null;
    }
}

export function hasContentChanged(title, body, dialect) {
    return (
        state.$savedTitle$ === title &&
        state.$savedBody$ === body &&
        state.$savedDialect$ === dialect
    );
}

export function getCredentials() {
    const credentials = _('#credentials');
    const email = credentials.dataset.email;
    const hash = credentials.dataset.hash;
    const docId = credentials.dataset.id;
    const authorId = credentials.dataset.author_id;
    credentials.remove();

    return {
        email: email,
        hash: hash,
        docId: docId,
        authorId: authorId,
    };
}
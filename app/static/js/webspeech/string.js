import { REPLACEMENTS } from './constants.js';
/**
 * The string module contains function for improving and editing strings
 * @module webspeech/string
 */

export { replacePlaceholders, splitParagraphs, };
/**
 * Replaces words
 * @param {String} string - the string where words should be replaced
 * @returns {String} the new string
 */
function replacePlaceholders (string, replacements=REPLACEMENTS) {
    let res = string;
    for (const replacement of replacements) {
        const regex = new RegExp(replacement[0], 'gim');
        res = res.replace(regex, replacement[1]);
    }
    return res;
}

/**
 * Splits up a string in several paragraphs, a paragraph is denoted by \n\n
 * @param {String} string the string to split up
 * @returns {array} an array of broken up strings
 */
function splitParagraphs(string) {
    console.log('splitParagraphs Dummy');
    // probably add </p><p>
    return string // string.split('\n\n');
}

/**
 * Improves the final transcript with capitlization and trimming 
 * and insert it into the #body
 * @param {string} transcript - the initial final transcript
 * @returns {string} - the improved final transcript
 */
export function improve_final_transcript(transcript) {
    let final_transcript = transcript
    final_transcript = final_transcript.replace(/&nbsp;/gm, '');
    //final_transcript = addSpaceAfterPunctuation(final_transcript);
    final_transcript = capitalize(final_transcript);
    return final_transcript;
}
  
/**
 * Capitilizes the first letter in a string. Ignores leading space.
 * @param {String} string
 * @returns {String} 
 */
export function capitalizeFirstLetter(string) {
    if (string.charAt(0) === ' ') // to ignore whitespace
        return (string.charAt(0) + 
                        string.charAt(1).toUpperCase() + 
                        string.slice(2));
    return string.charAt(0).toUpperCase() + string.slice(1);
}
  
export function capitalize(str) {
    return str.replace(/.+?[.?!](\s|$)/gm, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1);
    });
}

function addSpaceAfterPunctuation(str) {
    return str.replace(/[.?!:,;][a-zA-Z0-9]/gm, (match, offset, string) => {
        return string.substring(0, offset + 1) + ' ' + string.substring(offset + 1);
    });
}
import {LANG_DATA} from './langs.js'

export const LANGS = JSON.parse(LANG_DATA);
/* [['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]]; */


/**
 * A Transcript is an object containing:
 * @param {String} pre - the left side of the cursor
 * @param {String} post - the right side of the cursor
 * 
 * Don't access directly, only with helper functions
 * 
 */
let final_transcript = {
     pre:  "",
     post: ""
 }

/** 
 * sets the value of final_transcript.pre to the given string
 * @param {String} s - the new value for final_transcript.pre
 * @return {String} - the new value for final_transcript.pre
 */
export function setPre(s) {
    final_transcript.pre = s;     
    return final_transcript.pre;
}

/** 
 * sets the value of final_transcript.post to the given string
 * @param {String} s - the new value for final_transcript.post
 * @return {String} - the new value for final_transcript.post
 */
export function setPost(s) {
    final_transcript.post = s;
    return final_transcript.post;
}

/** 
 * returns the value of final_transcript.pre
 */
export function pre() {    
    return final_transcript.pre;
}

/** 
 * returns the value of final_transcript.post
 */
export function post() {
   return final_transcript.post;
}

/**
 * adds a string at the cursor position
 * @param {String} s - the string to add
 */
export function add(s) {
    return setPre(pre() + s);
}

 /** 
  * returns the combined final_transcript
  */
 export function finalTranscript() {
    return final_transcript.pre + final_transcript.post;
}

/**
 * consumes a textarea and produces an Transcript
 * @param {textarea} ta - the textarea to process
 */
export function valueToTranscript(ta) {
    return {
        pre: ta.value.slice(0, ta.selectionEnd),
        post: ta.value.slice(ta.selectionEnd)
    };
}

/**
 * consumes a Transcript and set the final_transcript accordingly,
 * @param {Transcript} ts - the Transcript to set
 * @returns {Transcript} - or Null if ts is not a {Transcript}
 */
export function setFinalTranscript(ts) {
    if (!isTranscript(ts)) {
        return null
    } 
    final_transcript = ts;
    return final_transcript;
}

/**
 * Consumes an Object and tests if it is a Transcript
 * @param {Object} obj - the object to test
 * @returns {Boolean} 
 */
function isTranscript(obj) {
    if (!obj.hasOwnProperty('pre') || !obj.hasOwnProperty('post')) {
        // TODO: Make this work with QUnit
        // throw new TypeError("TypeError: ts is not a Transcript");
        return false;
    } 
    return true;
}
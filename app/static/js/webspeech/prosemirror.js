import { last, _ } from '../util.js';
import {
    $,
    PMMD,
    TITLE,
    MARKED,
    EDITOR,
    PM,
    PMView,
    PMState,
    PMExampleSetup,
    EXPORT_BUTTON
} from './constants.js';

import {
    paragraphButton,
    colonButton,
    commaButton,
    periodButton
} from './punctuationHandlers.js';

import { state } from './state.js';

import { onClickStartButton } from './speechRecognition.js';

/**
 * The editor module contains function for editing and improving the
 * textarea
 * @module webspeech/editor
 */

//const PMTextSelection = PM.state.TextSelection;

export {
    createEditor,
    beyondGrammarPlugin,
    placeholderPlugin,
    setupProseMirror,
    insertParagraph,
    isCapital,
    needsSpace,
    activatePWA,
    deactivatePwa,
    moveMenu
};

// TODO refactor createEditor, beyondGrammarPlugin, placeholderPlugin that the can replace setupProseMirror

function beyondGrammarPlugin() {
    // init prosemirror

    const createBeyondGrammarPluginSpec =
        window['BeyondGrammar'].createBeyondGrammarPluginSpec;

    const spec = createBeyondGrammarPluginSpec(
        PM,
        document.getElementById('editor'),
        {
            service: {
                apiKey: '0AAB4A28-47F4-43F2-A474-7F6B172810C7',
                serviceUrl: '//rtg.prowritingaid.com'
            },
            grammar: {}
        }
    );
    return new PMState.Plugin(spec);
}

function placeholderPlugin(text) {
    return new PMState.Plugin({
        props: {
            decorations(state) {
                let doc = state.doc;
                if (
                    doc.childCount == 1 &&
                    doc.firstChild.isTextblock &&
                    doc.firstChild.content.size == 0
                )
                    return PMView.DecorationSet.create(doc, [
                        PMView.Decoration.widget(
                            1,
                            document.createTextNode(text)
                        )
                    ]);
            }
        }
    });
}

/**
 * Create an prosemMirror instance with the given plugins
 * @param {Array} plugins
 * @return {Object} ProseMirrorView
 */
function createEditor(content, beyondGrammarPlugin, placeholderPlugin) {
    const editor = new PMView.EditorView(document.getElementById('editor'), {
        state: PMState.EditorState.create({
            doc: PMMD.defaultMarkdownParser.parse(content),
            /* doc: prosemirrorModel.DOMParser
					.fromSchema(mySchema)
					.parse(document.getElementById("content")), */
            plugins: PMExampleSetup.exampleSetup({ schema: PMMD.schema })
                .concat(placeholderPlugin)
                .concat(beyondGrammarPlugin)
        })
    });

    //spec.editorView = editor;

    //console.log(PMMD.defaultMarkdownSerializer.serialize(spec.editorView.state.doc))
    return editor;
}

/**
 * Checks if the a space should be inserted into the editor
 * @param {prosemirror-state} state
 * @returns {boolean}
 */
function needsSpace(state) {
    const { $from, to } = state.selection;
    if ($from.parent.isTextblock) {
        const textContent = $from.parent.textContent;
        const parentOffset = $from.parentOffset;
        const charAtOffset = textContent[parentOffset];
        const char1BeforeOffset = textContent[parentOffset - 1];
        const char2BeforeOffset = textContent[parentOffset - 2];
        if (!textContent || parentOffset === 0) {
            return false;
        } else if (char1BeforeOffset === ' ') {
            return false;
        }
    }
    return true;
}

/**
 * checks if the next character at the current selection should be capitilized
 * @param {prosemirror-state} state the state where the current selection belongs
 */
function isCapital(state) {
    const { $from, to } = state.selection;
    if ($from.parent.isTextblock) {
        const textContent = $from.parent.textContent;
        const parentOffset = $from.parentOffset;
        const charAtOffset = textContent[parentOffset];
        const char1BeforeOffset = textContent[parentOffset - 1];
        const char2BeforeOffset = textContent[parentOffset - 2];
        if (!textContent || parentOffset === 0) {
            console.log('CAPITAL');
            return true;
        } else if (
            (['.', '!', '?'].includes(char2BeforeOffset) &&
                char1BeforeOffset === ' ') ||
            ['.', '!', '?'].includes(char1BeforeOffset)
        ) {
            console.log('CAPITAL');
            return true;
        }
    }
    return false;
}
window.isCapital = isCapital;

/**
 * Inserts a paragraph in the given EDITOR
 * @param {EditorState} state e.g. EDITOR.state
 * @param {fn} dispatch a dispatch function e.g. EDITOR.dispatch
 * @param {EditorView} view e.g. EDITOR
 * @returns {bool} - true if one of chainCommands got executed
 */
function insertParagraph(state, dispatch, view) {
    const fn = PM.commands.chainCommands(
        PM.commands.newlineInCode,
        PM.commands.createParagraphNear,
        PM.commands.liftEmptyBlock,
        PM.commands.splitBlock
    );
    return fn(state, dispatch, view);
}

export function setupEditor(content, placeholder) {
    EXPORT_BUTTON.addEventListener('click', onClickExportButton);
    document.addEventListener('keyup', onKeyUp);

    return setupProseMirror(content, placeholder);
}

function setupProseMirror(content, placeholder) {
    // init prosemirror

    const createBeyondGrammarPluginSpec =
        window['BeyondGrammar'].createBeyondGrammarPluginSpec;

    const spec = createBeyondGrammarPluginSpec(
        PM,
        document.getElementById('editor'),
        {
            service: {
                apiKey: '0AAB4A28-47F4-43F2-A474-7F6B172810C7',
                serviceUrl: '//rtg.prowritingaid.com'
            },
            grammar: {}
        }
    );

    const pwaPlugin = new PMState.Plugin(spec);

    function placeholderPlugin(text) {
        return new PMState.Plugin({
            props: {
                decorations(state) {
                    let doc = state.doc;
                    if (
                        doc.childCount == 1 &&
                        doc.firstChild.isTextblock &&
                        doc.firstChild.content.size == 0
                    )
                        return PMView.DecorationSet.create(doc, [
                            PMView.Decoration.widget(
                                1,
                                document.createTextNode(text)
                            )
                        ]);
                }
            }
        });
    }

    const editor = new PMView.EditorView(document.getElementById('editor'), {
        state: PMState.EditorState.create({
            doc: PMMD.defaultMarkdownParser.parse(content),
            /* doc: prosemirrorModel.DOMParser
					.fromSchema(mySchema)
					.parse(document.getElementById("content")), */
            plugins: PMExampleSetup.exampleSetup({ schema: PMMD.schema })
                .concat(placeholderPlugin(placeholder))
                .concat(pwaPlugin)
        })
    });

    spec.editorView = editor;

    moveMenu();
    //console.log(PMMD.defaultMarkdownSerializer.serialize(spec.editorView.state.doc))
    return editor;
}

/**
 * Removes the pwaPlugin from the given editor
 * @param {prosemirror} editor
 */
function deactivatePwa(editor) {
    if (
        last(editor.state.plugins).spec.constructor.name ===
        'BeyondGrammarProseMirrorPlugin'
    ) {
        editor.state.plugins.pop();
    }
}

/**
 * Adds the given pwaPlugin to the given editor
 * @param {prosemirror} editor
 * @param {prosemirror-plugin} pwa
 */
function activatePWA(editor, pwa) {
    return
    if (
        last(editor.state.plugins).spec.constructor.name !==
        'BeyondGrammarProseMirrorPlugin'
    ) {
        editor.state.plugins.push(pwa);
    }
}

/**
 * Move the Menubar left for 260px that it
 */
function moveMenu() {
    let menubar = _('.ProseMirror-menubar')[0];
    const oldLeft = parseInt(
        menubar.style.left.slice(0, menubar.style.left.length - 2)
    );
    const newLeft = oldLeft - 260;
    menubar.style.left = `${newLeft}px`;
}

/***********
 * Helpers *
 ***********/
/**
 * Enable the textarea of the editors textarea
 * @param {object} - a prosemirror editor
 */
export function enableBody() {
    EDITOR.dom.setAttribute('contenteditable', 'true');
    EDITOR.dom.classList.remove('not-allowed');
    EDITOR.focus();
}

/**
 * Disable the textarea of the editors textarea
 * @param {object} - a prosemirror editor
 */
export function disableBody() {
    EDITOR.dom.setAttribute('contenteditable', 'false');
    EDITOR.dom.classList.add('not-allowed');
    EDITOR.focus();
}

/**
 * Change the visibility of the export button and it's popup
 * @param {object} ev - The click event
 */
export function onClickExportButton() {
    const body = PMMD.defaultMarkdownSerializer.serialize(EDITOR.state.doc);
    const title = TITLE.value.length > 0 ? '# ' + TITLE.value + '\n' : '';
    const popup = $('#export_popup').modal({
        centered: false
    });
    popup.modal('show');
    const ta = _('#export_popup textarea');
    //const menu = _('.ProseMirror-menubar')[0];

    MARKED.setOptions({
        sanitize: true
    });
    ta.value = MARKED(title + body);

    //menu.classList.toggle('zIndex0');
    ta.select();
}

export function onKeyUp(ev) {
    const isMac = navigator.platform.indexOf('Mac') !== -1 ? true : false;
    const ky = ev.keyCode;
    const key = ev.key;
    const ctrlPressed = isMac ? ev.metaKey : ev.ctrlKey;
    // const altPressed = isMac ? ev.ctrlKey : ev.altKey;
    // const shiftPressed = ev.shiftKey;

    if (state.$recognizing$) {
        // not necessary at the moment
        // let isMac = ((process.platform === "darwin") ? true : false);

        switch (key) {
        case '.':
        case 'ArrowUp': //Up, because of presenter
            periodButton();
            ev.preventDefault();
            break;

        case ',':
        case 'ArrowDown': // down, on presenter
            commaButton();
            ev.preventDefault();
            break;

        case 'Enter': // the same on presenter
            paragraphButton();
            ev.preventDefault();
            break;

        case ':': // there is no Colon button
        case 'Tab': // tab on presenter
            colonButton();
            ev.preventDefault();
            break;

        case '?':
            console.log('Questionmark');
            // TODO questionButton();
            break;

        case '!':
            console.log('Exclamationmark');
            // TODO exclamationButton();
            break;

        case ';':
            console.log('semicolon');
            // TODO semicolonButton();
            break;

        default:
            break;
        }
    }

    if (ky === 32 && ctrlPressed) {
        onClickStartButton(ev);
        ev.preventDefault();
    }
    // reactivate if you develop new shortcuts
    // console.log(ky);
    // console.log(key);
}

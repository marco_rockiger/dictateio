import { RECOGNITION } from './constants.js';
import { state } from './state.js';

/******************************
 * export functions for punctutation *
 ******************************/

/**
 * Adds a punctuation sign to the final_transcript. Has sideeffects.
 * @param {string} punc - The punctuation sign.
 * @param {string} leading - leading whitespace, should one space or none
 * @param {string} trailing - trailing whitespace, should one space or none
 */
export function punctuationButton(punc, leading = '', trailing = ' ') {
    state.$punctuation$ = leading + punc + trailing;
    RECOGNITION.stop();
}

/**
 * Add a paragraph (\n\n) to the final_transcript
 * @param {object} ev - The click event
 */
export function paragraphButton() {
    punctuationButton('\n\n', '', '');
}

/**
 * Add a period to the final_transcript.
 * @param {object} ev - The click event.
 */
export function periodButton() {
    state.$capitalize$ = true;
    punctuationButton('.');
}

/**
 * Add a comma to the final_transcript.
 * @param {object} ev - The click event.
 */
export function commaButton() {
    punctuationButton(',');
}

/**
 * Add a colon to the final_transcript.
 * @param {object} ev - The click event.
 */
export function colonButton() {
    punctuationButton(':');
}



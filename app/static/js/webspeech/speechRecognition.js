import { _, } from '../util.js';
import { state, } from './state.js';
import {
    $,
    START_BUTTON,
    EDITOR,
    INTERIM_SPAN,
    INTERIM_WRAPPER,
    REVIEW_HELP,
    DICTATE_HELP,
    REPLACEMENTS,
    SELECT_LANGUAGE,
    SELECT_DIALECT,
    SETTINGS_BUTTON,
    // PWA,
    RECOGNITION,
    INFO_BUTTON,
} from './constants.js';
import { LANGS, } from './data.js';
// import { insertParagraph, activatePWA, } from './editor.js';
import {activatePwa, deactivatePwa} from './beyondgrammar-tinymce/beyond-grammar-plugin.js';
import { closeSearch } from './tinymce-dictandu-plugin/tinymce-dictandu.plugin.js'

import {
    capitalizeFirstLetter,
    replacePlaceholders,
    splitParagraphs,
    capitalize,
    improve_final_transcript,
} from './string.js';

import { needsSpace, enableBody, disableBody, isCapital } from './tinymce.js'
// import {
//     isCapital,
//     enableBody,
//     disableBody,
//     deactivatePwa,
//     needsSpace,
// } from './editor_old.js';

// start webkitSpeechRecognition setup
export function setupSpeechRecognition() {
    if (!('webkitSpeechRecognition' in window)) {
        upgrade();
        return null;
    } else {
        SETTINGS_BUTTON.addEventListener('click', onClickSettingsButton);
        INFO_BUTTON.addEventListener('click', onClickInfoButton);
        SELECT_LANGUAGE.addEventListener('change', updateCountry);
        SELECT_DIALECT.addEventListener('change', updateDialect);
        START_BUTTON.addEventListener('click', onClickStartButton);

        const recognition = new window.webkitSpeechRecognition();
        recognition.continuous = state.$continuous$;
        recognition.interimResults = state.$interimResults$;

        recognition.onstart = onStart();

        recognition.onerror = onError();

        recognition.onend = onEnd(recognition);

        recognition.onresult = onResult();

        recognition.onaudiostart = function() {};

        recognition.onsoundstart = function() {};

        recognition.onspeechstart = function() {};

        recognition.onaudioend = function() {};

        recognition.onsoundend = function() {};

        recognition.onspeechend = function() {};

        recognition.onnomatch = function() {
            console.log('ONNOMATCH');
        };

        return recognition;
    }
}

function onResult() {
    return function(event) {
        console.log('ONRESULT');
        let interim_transcript = '';
        for (let i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                if (state.$capitalize$ || isCapital(EDITOR.activeEditor.selection)) {
                    insertResult(
                        capitalizeFirstLetter(improve_final_transcript(event.results[i][0].transcript))
                    );
                    state.$capitalize$ = false;
                } else {
                    insertResult(improve_final_transcript(event.results[i][0].transcript));
                }
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        INTERIM_SPAN.innerHTML = interim_transcript;
    };
}

function onEnd(recognition) {
    return function() {
        console.log('ONEND');
        console.log('state.$restart$:', state.$restart$);
        console.log('state.$ignore_onend$:', state.$ignore_onend$);
        console.log('End ONEND');
        if (state.$punctuation$ !== '') {
            if (state.$punctuation$ === '\n\n') {
                insertParagraph(EDITOR.state, EDITOR.dispatch, EDITOR);
            } else {
                insertResult(state.$punctuation$);
            }
            if (
                state.$punctuation$ === '.' ||
                state.$punctuation$ === '!' ||
                state.$punctuation$ === '?' ||
                '\n\n'
            ) {
                state.$capitalize$ = true;
            }
            state.$punctuation$ = '';
            recognition.start();
        } else if (state.$restart$) {
            recognition.start();
            return;
        } else {
            const content = EDITOR.activeEditor.getContent();
            const improvedResult = improve_final_transcript(content);
            EDITOR.activeEditor.setContent('');
            EDITOR.activeEditor.insertContent(improvedResult);

            EDITOR.activeEditor.focus();
        }
        //$recognizing$ = false;
        if (state.$ignore_onend$) {
            //return;
        }
        toStartButton();
        showInfo('onend');
        if (EDITOR.activeEditor.getContent().length === 0) {
            return;
        }
    };
}

function onStart() {
    return function() {
        console.log('ONSTART');
        state.$replacements$ = getReplacements();
        state.$recognizing$ = true;
        toStopButton();
    };
}

// TODO check for more events of recognition
// https://dvcs.w3.org/hg/speech-api/raw-file/9a0075d25326/speechapi.html#speechreco-events
// https://developer.mozilla.org/en-US/docs/Web/API/SpeechRecognition
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API
// https://developer.mozilla.org/en-US/docs/Web/Events/audioend
function onError() {
    return function(event) {
        console.log('ONERROR');
        console.log(event);
        if (event.error == 'no-speech') {
            // restart recognition if the user doesn't speaks
            console.log('no-speech');
            if (state.$recognizing$) state.$restart$ = true;
        }
        if (event.error == 'audio-capture') {
            toStartButton();
            showInfo('info_no_microphone');
            state.$ignore_onend$ = true;
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - state.$start_timestamp$ < 100) {
                showInfo('info_blocked');
            } else {
                showInfo('info_denied');
            }
            state.$ignore_onend$ = true;
        }
        if (event.error == 'network') {
            console.warn('Network error, restarting...');
            if (state.$recognizing$) state.$restart$ = true;
        }
    };
}

function upgrade() {
    START_BUTTON.style.visibility = 'hidden';
    showInfo('info_upgrade');
}

function showInfo(s) {
    switch (s) {
    case 'info_no_microphone':
        alert(
            'No microphone was found. Ensure that a microphone is installed and that\n<a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">microphone settings</a> are configured correctly.'
        );
        break;

    case 'info_allow':
        alert('Click the "Allow" button above to enable your microphone.');
        break;

    case 'info_denied':
        alert('Permission to use microphone was denied.');
        break;

    case 'info_blocked':
        alert(
            'Permission to use microphone is blocked. To change, go to chrome://settings/contentExceptions#media-stream'
        );
        break;

    case 'info_upgrade':
        alert(
            'Web Speech API is not supported by this browser.\nUpgrade to <a href="//www.google.com/chrome">Chrome</a>\nversion 25 or later.'
        );
        break;

    default:
        console.log('SHOW INFO:', s);
        break;
    }
}

/**
 * Add a slashed mic to the start button and change the text
 * to Stop Recording
 */
function toStopButton() {
    START_BUTTON.innerHTML =
        '<i class="fa fa-microphone-slash" aria-hidden="true"></i>\n<span class="button-text">Stop Recording</span>';
    START_BUTTON.classList.remove('green');
    START_BUTTON.classList.add('red');
    disableBody(); 
    if (state.$interimResults$) INTERIM_WRAPPER.classList.remove('hidden');
}

/**
 * Add a mic to the start button and change the text
 * to Start Recording
 */
function toStartButton() {
    START_BUTTON.innerHTML =
        '<i class="fa fa-microphone" aria-hidden="true"></i>\n<span class="button-text">Start Recording</span>';
    START_BUTTON.classList.remove('red');
    START_BUTTON.classList.add('green');
    enableBody();
    INTERIM_WRAPPER.classList.add('hidden');
}

/**
 * Get all the replacements the user entered in inverted ranking
 * @return {Array} - [[<phrase>, <replacement>], ...]
 */
function getReplacements() {
    const substitutions = document.querySelectorAll(
        '#automatic-substitution .my-substitution__row'
    );
    const replacements = [];

    // cicle backwards through the array, except of the firs two lines, because the don't contain any values
    for (let i = substitutions.length - 1; i > 1; i--) {
        // we need a jquery object here
        const substitution = substitutions[i];
        const checkbox = $(substitution.children[0].children[0].children[0]);
        const isChecked = checkbox.checkbox('is checked');
        if (isChecked) {
            const placeholder =
                substitution.children[0].children[1].children[0];
            const replacement =
                substitution.children[0].children[2].children[0];
            replacements.push([placeholder.value, replacement.value,]);
        }
    }
    return replacements;
}

export function insertResult(str) {
    const result = replacePlaceholders(
        str,
        REPLACEMENTS.concat(state.$replacements$)
    );
    const capitalized = capitalize(result);
    const paragraphs = EDITOR.activeEditor.insertContent(capitalized);
    // for (let i = 0; i < paragraphs.length; i++) {
    //     const paragraph = paragraphs[i];

    //     const oldTr = EDITOR.state.tr;
    //     const newTr = oldTr.insertText(paragraph);
    //     newTr.scrollIntoView();
    //     EDITOR.dispatch(newTr);

    //     if (i + 1 < paragraphs.length)
    //         insertParagraph(EDITOR.state, EDITOR.dispatch, EDITOR);
    // }
}

/** Setup the language */
export function setupLanguage() {
    for (let i = 0; i < LANGS.length; i++) {
        SELECT_LANGUAGE.options[i] = new Option(LANGS[i][0], i);
    }

    const { selectedLanguage, selectedDialect, } = getSelectedLanguage();

    SELECT_LANGUAGE.selectedIndex = selectedLanguage;
    updateCountry({}, false);
    SELECT_DIALECT.selectedIndex = selectedDialect;

    function getSelectedLanguage() {
        const dialect = _('#dialect');
        const langStr = dialect.value;
        for (let i = 0; i < LANGS.length; i++) {
            for (let j = 1; j < LANGS[i].length; j++) {
                if (LANGS[i][j][0] === langStr) {
                    return {
                        selectedLanguage: i,
                        selectedDialect: --j,
                    };
                }
            }
        }
    }
    // American English as default or fallback
    return {
        selectedLanguage: 6,
        selectedDialect: 6,
    };
}

export function updateCountry(ev, toUpdateDialect = true) {
    for (let i = SELECT_DIALECT.options.length - 1; i >= 0; i--) {
        SELECT_DIALECT.remove(i);
    }
    const list = LANGS[SELECT_LANGUAGE.selectedIndex];
    for (let i = 1; i < list.length; i++) {
        SELECT_DIALECT.options.add(new Option(list[i][1], list[i][0]));
    }

    SELECT_DIALECT.disabled = list[1].length == 1 ? true : false;
    if (toUpdateDialect) {
        updateDialect();
    }
}

export function updateDialect() {
    const dialect = _('#dialect');
    const langStr = dialect.value;

    const dialectInForm =
        SELECT_DIALECT.options[SELECT_DIALECT.selectedIndex].value; // DEBUG
    // TODO

    if (langStr !== dialectInForm) {
        dialect.value = dialectInForm;
        console.warn('Changed dialect');
        console.log(dialectInForm);

        const isActivatedPwa = dialect.value.startsWith('en-') ? true : false;
        if (isActivatedPwa) {
            activatePwa(EDITOR);
            console.log('activate PWA');
        } else {
            deactivatePwa(EDITOR);
            console.log('deactivate PWA');
        }
    }
}

/**
 * Change the visibility of the setting button and it's popup
 * @param {object} ev - The click event
 */
export function onClickSettingsButton() {
    const popup = $('#settings_popup').modal({
        centered: false,
    });
    popup.modal('show');
}

export function onClickInfoButton() {
    $('#info_button')
  .popup({
      html: `
      <div class="ui mini statistic">
        <div class="label">
          Words
        </div>
        <div class="value">
        ${tinymce.activeEditor.plugins.wordcount.getCount()}
        </div>
      </div>`,
      on: 'click',
  }).popup('show');
}

export function onClickStartButton(event) {
    if (state.$recognizing$) {
        state.$restart$ = false;
        state.$ignore_onend$ = false;
        state.$recognizing$ = false;
        RECOGNITION.stop();
        enableBody();
        return;
    }

    if (EDITOR.activeEditor.getContent().length === 0) {
        state.$capitalize$ = true;
    }

    if (needsSpace(EDITOR.activeEditor.selection)) {
        insertResult(' ');
    }

    state.$recognizing$ = true;
    state.$restart$ = true;
    state.$ignore_onend$ = true;
    state.$ignore_onend$ = true; // could be the problem
    RECOGNITION.lang = SELECT_DIALECT.value;
    RECOGNITION.start();
    document.getElementById('mceu_11-button').click()   // To close the search
    INTERIM_SPAN.innerHTML = '';
    state.$start_timestamp$ = Date.now();

    disableBody();
}

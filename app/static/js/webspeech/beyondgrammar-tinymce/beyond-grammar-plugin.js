import { $, EDITOR } from '../constants.js'
import { _, } from '../../util.js';
import { TinyMCESettingsWindowFactory } from './TinyMCESettingsWindowFactory.js';
import {sanitizeHtmlFromQuery} from "./sanitizeHtmlFromQuery.js"; // TODO
import {tinymceTranslate as t} from "./tinymceTranslate.js";

// const Editor = EDITOR.Editor;
// const Button = EDITOR.Button;
// const ButtonSettings = EDITOR.Button;
// const Event = EDITOR.Event;
// const PasteEvent = EDITOR.PasteEvent;
// const GetContentEvent = EDITOR.GetContentEvent;
// const SetContentEvent = EDITOR.SetContentEvent;

// TODO require('style!css!./styles/tinymce-plugin-styles.css'); 

export function setupPWA() {
	console.log('setupPWA:');
tinymce.PluginManager.add('BeyondGrammar', function(editor) {

    let rawSettings = editor.settings.bgOptions || { service : {}, grammar : {} };
    let serviceSettings = tinymce.util.Tools.extend({
        sourcePath : '//cdn.prowritingaid.com/beyondgrammar/release/dist/hayt/bundle.js',
        serviceUrl : '//rtg.prowritingaid.com',
        i18n       : {en : "./libs/i18n-en.js"}
    }, rawSettings.service );

    let plugin;
    let alreadyLoaded = false;
    
    editor.addButton('BeyondGrammar', {
        icon: 'beyond-grammar-toolbar-icon-16',
        onpostrender : (e)=>{ },
        onclick : () => plugin.openSettingsWindow()        
    });

    editor.on('init', () => {
        if( alreadyLoaded ) {
            //protecting from word press twice init. https://tommcfarlin.com/wordpress-hooks-firing-twice/
            return;
        }
        alreadyLoaded = true;
        
        let scriptLoader = new tinymce.dom.ScriptLoader();
        //load grammar script
        scriptLoader.add( serviceSettings.sourcePath );

        //load provided i18n files
        for(let code in serviceSettings.i18n){
            scriptLoader.add( serviceSettings.i18n[code], ((code)=>
                    ()=>tinymce.util.I18n.add( code, window[`GrammarChecker_lang_${code}`].default)
            )(code) );
        }

        //editor.setProgressState(true);
        scriptLoader.loadQueue(()=>{
            //editor.setProgressState(false);
            let GrammarChecker = window['BeyondGrammar'].GrammarChecker;
            let element = editor.getBody();
			let checker = new GrammarChecker(element, serviceSettings, rawSettings.grammar);
			// make checker globaly available
			window.checker = checker;
			// end
            plugin = new BeyondGrammarPlugin( editor, checker );
        });
    });

    class BeyondGrammarPlugin {

        constructor(editor, grammarChecker){
			this.editor = editor;
			this.grammarChecker = grammarChecker;

            grammarChecker.init()
                .then(()=>this.startPlugin())
                .catch((error)=>{
                    // some errors on init
                    console.log('BeyondGrammarChecker', error);
                });
        }

        startPlugin() {
            this.uiFactory = new TinyMCESettingsWindowFactory();

            this.bindPastePatching();
            this.bindContentChangeBehavior();

            window['BeyondGrammar'].sanitizeHtmlFromQuery = sanitizeHtmlFromQuery;

            const isActivatedPwa = _('#dialect').value.startsWith('en-') ? true : false;
            if( this.grammarChecker.getSettings().checkerIsEnabled && isActivatedPwa) {
                this.grammarChecker.activate();
            }
        }

        bindPastePatching() {
            this.editor.on('PastePreProcess', (e)=>{
                e.content = sanitizeHtmlFromQuery('span.pwa', e.content);
            });
        }

        bindContentChangeBehavior() {
            this.editor.on('GetContent', (e)=>{
                if( e.format == 'html' ) {
                    e.content = sanitizeHtmlFromQuery('span.pwa-mark', e.content);
                }
            });
            
            this.editor.on("SetContent", (e)=>{
                if(e.format == 'html') {
                    this.grammarChecker.checkAll();
                }
            });
        }

        openSettingsWindow(activeTab = 0) {
            let descriptorObj = this.uiFactory.createSettingsWindow( 500, 300, this.grammarChecker, activeTab);
            let settingsWindow = editor.windowManager.open( descriptorObj );            
            let $win = $(settingsWindow.$el[0]);            
            let dictionaryController = new DictionaryController(editor,settingsWindow,  this.grammarChecker, $win, false);
            
            settingsWindow.on('submit', (e)=>{
                this.grammarChecker.setSettings( e.data );                
                dictionaryController.destroy();
            });
        }
    }
    
    class DictionaryController {
        // private PREFIX : string;        
        // private $listBox : JQuery;        
        // private mce_entryInput : any;
        // private mce_replaceWithInput : any;
        // private mce_addButton : any;
        // private mce_deleteButton : JQuery;
        
        // private lastLoadedEntries : DictionaryEntry[] = [];
        
        constructor(editor, settingsWindow, grammarChecker, $window, isReplace) {

			this.editor = editor;
			this.settingsWindow = settingsWindow;
			this.grammarChecker = grammarChecker;
			this.$window = $window;
			this.isReplace = isReplace;

            this.PREFIX = isReplace ? "replace" : "dictionary";
            this.initUI();
            this.reloadDictionary(true).catch(()=>{
                this.activateUI();
            })
        }
        
        initUI(){
            let byId = (id)=>this.settingsWindow.find(`#${id}`)[0];
            
            this.mce_addButton    = byId(`${this.PREFIX}-add-button`);
            this.mce_deleteButton = byId(`${this.PREFIX}-delete-button`);
            this.mce_entryInput = byId(`${this.PREFIX}-entry-textbox`);
            this.mce_replaceWithInput = byId(`${this.PREFIX}-replace-textbox`);

            let $itemsContainer = $(this.settingsWindow.$el[0]).find(`#${this.PREFIX}-contents-container>.mce-container-body`);
            this.$listBox = $("<select>")
                .css({ 
                    width : "385px", overflow : "auto", border : "1px solid #ccc7c7", boxSizing : "border-box",
                    height : "185px", "padding" : 0, lineHeight : "initial"
                })
                .attr({ size : 8 })
                .appendTo( $itemsContainer );
            
            this.mce_addButton.on("click", ()=>this.addToDictionary() );
            this.mce_deleteButton.on("click", ()=>this.deleteFromDictionary() );
        }
        
        addToDictionary(){
            let word = this.mce_entryInput.value();
            let replaceWith = this.isReplace ? this.mce_replaceWithInput.value() : undefined;
            
            let errorMessage = this.getErrorMessageForAddToDictionary(word, replaceWith);
            if( errorMessage ) {
                this.editor.windowManager.alert(errorMessage);
                return;
            }
            
            this.mce_entryInput.value("");
            this.isReplace && this.mce_replaceWithInput.value("");
            
            this.activateUI(false );
            
            this.grammarChecker
                .addToDictionary(word, replaceWith)
                .then(()=>this.reloadDictionary())
                .then(()=>this.activateUI())
                .catch(()=>{
                    this.editor.windowManager.alert(t("beyond-loading-error"));
                    this.activateUI();
                })
        }
        
        deleteFromDictionary(){
            let id  = this.$listBox.val();
            
            let errorMessage = this.getErrorMessageForDeleteFromDictionary(id);
            if( errorMessage ) {
                this.editor.windowManager.alert(errorMessage);
                return;
            }
            
            this.activateUI(false);
            
            this.grammarChecker
                .removeFromDictionary(id)
                .then(()=>this.reloadDictionary())
                .then(()=>this.activateUI())
                .catch(()=>{
                    this.editor.windowManager.alert(t("beyond-loading-error"));
                    this.activateUI();
                })
        }
        
        reloadDictionary(changeUI = false){
            if( changeUI )this.activateUI(false);
            
            return this.grammarChecker.getDictionaryEntries().then((entries)=>{
                let items = entries.filter(e=>this.isReplace ? !!e.Replacement : !e.Replacement);
                
                this.$listBox.empty().append( items.map((item)=>{
                    return $("<option>")
                        .attr("value", item.Id )
                        .css({padding : "3px"})
                        .text( `${item.Word}${ item.Replacement? ` => ${item.Replacement}` : "" }` )
                }));
                
                this.lastLoadedEntries = items;
                
                if(changeUI) this.activateUI();
                
                return items;
            });
        }
        
        activateUI(active = true){
            let state = (mce)=>{
                mce.active(active);
                mce.disabled(!active);
            };
            
            state(this.mce_addButton);
            state(this.mce_deleteButton);
        }

        /**
         * Returns error message if there is error and null if not
         * @param {string} word
         * @param {string} replaceWith
         * @returns {string}
         */
        getErrorMessageForAddToDictionary( word, replaceWith) {
            if( this.isReplace ) {
                if( !word || !replaceWith ){
                    return t("beyond-empty-terms-for-add-to-dictionary-replacements");
                }

                if( this.lastLoadedEntries.filter((e)=>e.Word.toLowerCase() == word.toLowerCase() && e.Replacement.toLowerCase() == replaceWith.toLowerCase()).length > 0 ){
                    return t("beyond-input-replacement-already-exists", word, replaceWith);
                }

                if( word == replaceWith ) {
                    return t("beyond-added-same-values-for-replace");
                }

            } else {
                if( !word ){
                    return t("beyond-empty-terms-for-add-to-dictionary-word");
                }

                if( this.lastLoadedEntries.filter((e)=>e.Word.toLowerCase() == word.toLowerCase()).length > 0 ){
                    return t("beyond-word-already-exists-in-dictionary", word);
                }
            }

            return null;
        }
        
        getErrorMessageForDeleteFromDictionary( id ) {
            if( !id ) {
                return t("beyond-item-to-deleted-is-not-selected");
            }
            
            return null;
        }
        
        destroy(){
            this.mce_addButton.off("click");
            this.mce_deleteButton.off("click");
        }
    }
    
});
}

export function deactivatePwa() {
	console.log('deactivatePWA');
	window.checker.deactivate();
}

export function activatePwa() {
	console.log('activatePWA');
	window.checker.activate();
}
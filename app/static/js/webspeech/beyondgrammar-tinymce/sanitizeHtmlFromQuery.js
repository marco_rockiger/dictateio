import { $ } from '../constants.js';

/**
 * 
 * @param {string} query 
 * @param {string} html 
 * @returns {string}
 */
export function sanitizeHtmlFromQuery( query, html ) {
    let $pseudo = $(`<div>${html}</div>`);
    let $items = $( $pseudo.find(query).get().reverse() );
    $items.contents().unwrap();
    return $pseudo.html();
}
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {module.exports = global["GrammarChecker_lang_en"] = __webpack_require__(12);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ }),

/***/ 12:
/***/ (function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = {
	    //Tabs
	    "beyond-language-tab-label": "Language",
	    "beyond-options-tab-label": "Options",
	    "beyond-dictionary-tab-label": "Dictionary",
	    "beyond-replacements-tab-label": "Replacements",
	    "beyond-about-tab-label": "About",
	    //Language tag
	    "beyond-check-spelling-label": "Check Spelling",
	    "beyond-check-grammar-label": "Check Grammar",
	    "beyond-check-style-label": "Check Style",
	    "beyond-select-language-label": "Language :",
	    //Options tab
	    "beyond-checker-is-enabled": "Grammar Checker is started",
	    "beyond-double-click-shows-thesaurus": "Double click shows thesaurus",
	    //Dictionary tab
	    "beyond-word-input-label": "Word",
	    "beyond-add-word-button-label": "Add",
	    "beyond-dictionary-contents-list-label": "Contents",
	    "beyond-dictionary-delete-button-label": "Delete",
	    //Replacements tab
	    "beyond-replace-input-label": "Replace",
	    "beyond-replace-with-input-label": "With",
	    "beyond-add-replacement-button-label": "Add",
	    "beyond-replacements-contents-list-label": "Contents",
	    "beyond-replacements-delete-button-label": "Delete",
	    //Common
	    "beyond-settings-window-header": "Settings {0} - v{1}",
	    //Errors
	    "beyond-loading-error": "An error occurred during loading. Please try again",
	    "beyond-empty-terms-for-add-to-dictionary-replacements": "Please enter a term to replace and a replacement value.",
	    "beyond-empty-terms-for-add-to-dictionary-word": "Please enter a word to add to the dictionary.",
	    "beyond-word-already-exists-in-dictionary": '"{0}" already exists in the dictionary',
	    "beyond-added-same-values-for-replace": "What is the sense to replace one word with the same?",
	    "beyond-input-replacement-already-exists": `Replacement "{0}=>{1}" already exists in the dictionary`,
	    "beyond-item-to-deleted-is-not-selected": "Please select item to delete"
	};


/***/ })

/******/ });
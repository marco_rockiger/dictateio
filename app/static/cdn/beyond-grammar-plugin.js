/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	//import * as $ from "jquery";
	var highlight_spec_1 = __webpack_require__(2);
	var utils_1 = __webpack_require__(5);
	var dom_1 = __webpack_require__(6);
	var CSS_IGNORED_ = 'pwa-mark-ignored';
	var PWA_DECO_UPDATE_META_ = 'pwa-deco-update';
	function createBeyondGrammarPluginSpec_(PM, element, bgOptions) {
	    var DEFAULT_SETTINGS = {
	        grammar: {},
	        service: {
	            sourcePath: "//cdn.prowritingaid.com/beyondgrammar/release/dist/hayt/bundle.js"
	        }
	    };
	    bgOptions.grammar = utils_1.objectAssign(DEFAULT_SETTINGS.grammar, bgOptions.grammar);
	    bgOptions.service = utils_1.objectAssign(DEFAULT_SETTINGS.service, bgOptions.service);
	    var $element = $(element);
	    var plugin = new BeyondGrammarProseMirrorPlugin($element, PM);
	    dom_1.loadBeyondGrammarModule_(bgOptions.service.sourcePath, function (bgModule) {
	        bgModule.loadPwaMarkStyles(window);
	        var $contentEditable = $element.find("[contenteditable]");
	        var grammarChecker = new bgModule.GrammarChecker($contentEditable[0], bgOptions.service, bgOptions.grammar, plugin);
	        grammarChecker.init().then(function () {
	            grammarChecker.activate();
	            plugin.bgModule_ = bgModule;
	        });
	    });
	    return plugin;
	}
	exports.createBeyondGrammarPluginSpec_ = createBeyondGrammarPluginSpec_;
	//idea to combine in one class two implementations: bg wrapper + pm plugin, so we can work in one scope
	var BeyondGrammarProseMirrorPlugin = (function () {
	    function BeyondGrammarProseMirrorPlugin($element_, PM_) {
	        this.$element_ = $element_;
	        this.PM_ = PM_;
	        this.isBound_ = false;
	        this.initState_();
	        this.initProps_();
	        this.bindEditableEvents_();
	    }
	    BeyondGrammarProseMirrorPlugin.prototype.bindEditableEvents_ = function () {
	        var _this = this;
	        this.$element_.on('scroll', function () {
	            // close the popup, otherwise it moves away from the word
	            if (_this.onPopupClose) {
	                _this.onPopupClose();
	            }
	        });
	        this.$element_.on('keydown', function (evt) {
	            // if we press a button in the text then close the popup
	            if (evt.keyCode == 17) {
	                //temporally solution for awhile we use ctrlKey for opening contextual thesaurus
	                return;
	            }
	            if (_this.onPopupClose) {
	                _this.onPopupClose();
	            }
	        });
	        this.$element_.on('mouseover touchend', ".pwa-mark:not(." + CSS_IGNORED_ + ")", function (evt) {
	            var elem = evt.target, uid = elem.getAttribute('data-pwa-id');
	            if (_this.onShowPopup) {
	                _this.onShowPopup(uid, elem);
	            }
	        });
	        this.$element_.on('mouseleave', '.pwa-mark', function () {
	            if (_this.onPopupDeferClose) {
	                _this.onPopupDeferClose();
	            }
	        });
	    };
	    /**
	     * Implementation of ProseMirror plugin interface
	     */
	    BeyondGrammarProseMirrorPlugin.prototype.initState_ = function () {
	        var self = this;
	        this.decos_ = self.PM_.view.DecorationSet.empty;
	        // noinspection JSUnusedLocalSymbols
	        this.state_ = {
	            init: function (config, state) {
	                // we should start the checker
	                //self.onCheckRequired();
	                // we do nothing here
	                self.doc_ = state.doc;
	                //console.log(self.doc);
	                return { decos: self.decos_ };
	            },
	            apply: function (tr, pluginState, old, newState) {
	                //console.log("apply value=", pluginState);
	                //storing new doc, as it was changed after transactions
	                self.doc_ = newState.doc;
	                if (tr.docChanged) {
	                    // I think we need to update our decos using the mapping of
	                    // the transaction. This should update all the from and tos
	                    //As I understand it makes sense only if content was changed, in other case it is not necessary
	                    self.decos_ = self.decos_.map(tr.mapping, self.doc_);
	                    self.decos_ = self.invalidateDecorations_(self.decos_);
	                }
	                if (tr.docChanged) {
	                    self.onDocChangedTransaction_(tr);
	                }
	                return { decos: self.decos_ };
	            }
	        };
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.onDocChangedTransaction_ = function (tr) {
	        var _this = this;
	        // get the range that is affected by the transformation
	        var range = this.rangeFromTransform_(tr);
	        // update all the blocks that have been affected by the transformation
	        this.doc_.nodesBetween(range.from, range.to, function (elem, pos) {
	            if (elem.isTextblock) {
	                if (_this.onBlockChanged) {
	                    //console.info("onBlockChanged", elem);
	                    _this.onBlockChanged(_this.wrapNode_(elem, pos));
	                }
	                return false;
	            }
	            return true;
	        });
	        // set off a check
	        if (this.onCheckRequired) {
	            this.onCheckRequired();
	        }
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.initProps_ = function () {
	        var self = this;
	        this.props_ = {
	            decorations: function () { return this.spec.decos_; },
	            attributes: { spellcheck: "false" },
	            handleDoubleClick: function () {
	                if (!self.isBound_) {
	                    return true;
	                }
	                setTimeout(function () {
	                    self.processShowContextualThesaurus_(null);
	                }, 10);
	                return false;
	            }
	        };
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.processShowContextualThesaurus_ = function ($target) {
	        if (!this.onShowThesaurus)
	            return false;
	        var thesaurusData = this.bgModule_.getThesaurusData(dom_1.getWindow_(this.$element_[0]), this.$element_, $target, true);
	        this.lastThesaurusData_ = thesaurusData;
	        return this.onShowThesaurus(thesaurusData, dom_1.getWindow_(this.$element_[0]));
	    };
	    Object.defineProperty(BeyondGrammarProseMirrorPlugin.prototype, "state", {
	        get: function () {
	            return this.state_;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(BeyondGrammarProseMirrorPlugin.prototype, "props", {
	        get: function () {
	            return this.props_;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    BeyondGrammarProseMirrorPlugin.prototype.invalidateDecorations_ = function (decos) {
	        var _this = this;
	        var changed = decos
	            .find()
	            .filter(function (deco) { return _this.doc_.textBetween(deco.from, deco.to) != deco.spec.word; });
	        return changed.length == 0 ? decos : decos.remove(changed);
	    };
	    /**
	     * Implementations of IEditableWrapper
	     */
	    BeyondGrammarProseMirrorPlugin.prototype.applyHighlightsSingleBlock = function (elem, text, tags, checkAll) {
	        // problem is PM is in most cases immutable, so if we started checking on one node, will type something
	        // in dom we will have another node, but as we have it returned from closure this node can be 
	        // incorrect(removed from dom structure). So we should make checking by existing checked element
	        // not it's content or text, as it is in dom, that means it is not changed, if it is not in dom, that
	        // means it is not actual and we can skip check result.
	        //unwrapping element, as elem is result of wrapNode method
	        //store textContent as tag positions related to it
	        var _a = elem, node = _a.node, textContent = _a.textContent;
	        var found = false;
	        this.doc_.descendants(function (n) {
	            if (node == n) {
	                found = true;
	            }
	            return !found;
	        });
	        if (!found)
	            return; //nothing to do
	        var start = this.getPositionInDocument_(node);
	        var length = text.length;
	        // find the decos from the start and end of this element and remove them
	        var decosForBlock = this.decos_.find(start, start + length);
	        var newDecos = [];
	        for (var i = 0; i < tags.length; i++) {
	            var tag = tags[i];
	            var tagPos = { from: tag.startPos + start, to: tag.endPos + start + 1 };
	            var existing = null;
	            for (var k = 0; k < decosForBlock.length; k++) {
	                var deco = decosForBlock[k];
	                var spec = deco.spec;
	                if (deco.from === tagPos.from && deco.to === tagPos.to) {
	                    //update tag item with new tag instance
	                    spec.tag = tag;
	                    // As I understand we should make step backward, as if we've removed on k, k+1 in next iteration
	                    // skips, as it was shifted
	                    decosForBlock.splice(k--, 1);
	                    existing = deco;
	                    break;
	                }
	            }
	            // no existing, so we can say it is new???
	            if (existing === null) {
	                // check for an existing decoration  
	                //
	                var word = textContent.substring(tag.startPos, tag.endPos + 1);
	                var spec = new highlight_spec_1.HighlightSpec(tag, word);
	                var attributes = utils_1.createDecorationAttributesFromSpec(spec);
	                var deco = this.PM_.view.Decoration.inline(tagPos.from, tagPos.to, attributes, spec);
	                newDecos.push(deco);
	            }
	        }
	        this.decos_ = this.decos_.remove(decosForBlock).add(this.doc_, newDecos);
	        this.applyDecoUpdateTransaction_();
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.getHighlightInfo = function (uid) {
	        var decos = this.decos_;
	        if (decos) {
	            var deco = this.getDecoById_(uid);
	            if (deco) {
	                return deco.spec.highlightInfo;
	            }
	        }
	        return null;
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.clearMarks = function (skipIgnored) {
	        if (skipIgnored) {
	            //find not ignored decos and remove only it
	            var notIgnoredDecos = this.decos_.find(undefined, undefined, function (spec) { return !spec.ignored; });
	            this.decos_ = this.decos_.remove(notIgnoredDecos);
	        }
	        else {
	            this.decos_ = this.PM_.view.DecorationSet.empty;
	        }
	        this.applyDecoUpdateTransaction_();
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.ignore = function (uid) {
	        var _this = this;
	        var deco = this.getDecoById_(uid);
	        if (deco) {
	            this.applyDecoUpdateTransaction_(function () {
	                //getting old spec, marking it as ignored and creating from it new ignored deco
	                var spec = deco.spec;
	                spec.ignored = true;
	                var new_deco = _this.PM_.view.Decoration.inline(deco.from, deco.to, utils_1.createDecorationAttributesFromSpec(spec), spec);
	                _this.decos_ = _this.decos_.remove([deco]).add(_this.doc_, [new_deco]);
	                return deco.to;
	            });
	        }
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.omit = function (uid) {
	        var _this = this;
	        var deco = this.getDecoById_(uid);
	        if (deco) {
	            this.applyDecoUpdateTransaction_(function (tr) {
	                tr.delete(deco.from, deco.to);
	                _this.decos_ = _this.decos_.remove([deco]);
	                return deco.from;
	            });
	        }
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.accept = function (uid, suggestion) {
	        var _this = this;
	        var deco = this.getDecoById_(uid);
	        if (deco) {
	            this.applyDecoUpdateTransaction_(function (tr) {
	                //let tr = this.editorView.state.tr;
	                _this.decos_ = _this.decos_.remove([deco]);
	                tr
	                    .replace(deco.from, deco.to)
	                    .insertText(suggestion, deco.from);
	                return deco.from + suggestion.length;
	            });
	        }
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.onAddToDictionary = function (uid) {
	        var _this = this;
	        var deco = this.getDecoById_(uid);
	        if (deco) {
	            this.applyDecoUpdateTransaction_(function () {
	                var specToAdd = deco.spec;
	                var decosToRemove = _this.decos_.find(null, null, function (spec) {
	                    return spec.tag.category == specToAdd.tag.category && spec.word == specToAdd.word;
	                });
	                _this.decos_ = _this.decos_.remove(decosToRemove);
	                return deco.to;
	            });
	        }
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.applyThesaurus = function (replacement) {
	        var _this = this;
	        this.applyDecoUpdateTransaction_(function (tr) {
	            tr.insertText(replacement, tr.selection.from, tr.selection.from + _this.lastThesaurusData_.word.length);
	            return tr.selection.from + _this.lastThesaurusData_.word.length;
	        });
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.getText = function (blockElement) {
	        var node = blockElement;
	        return node.textContent;
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.getAllElements = function () {
	        var _this = this;
	        var result = [];
	        this.doc_.descendants(function (node, pos) {
	            if (node.isTextblock) {
	                result.push(_this.wrapNode_(node, pos));
	                return false;
	            }
	            return true;
	        });
	        return result;
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.wrapNode_ = function (node, pos) {
	        // we should re-write text content, as in real case textContent of DOM can't contains images and unsized elements
	        // but PM can do this. So it's text block can contains images, so when we getting textContent we skips images
	        // and broken indexed after image when adding highlights
	        return {
	            node: node,
	            textContent: this.doc_.textBetween(pos, pos + node.nodeSize, "\n", "\n")
	        };
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.getCurrentErrorCount = function () {
	        return this.decos_.find().length;
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.applyDecoUpdateTransaction_ = function (process) {
	        var tr = this.editorView.state.tr;
	        tr.setMeta(PWA_DECO_UPDATE_META_, true);
	        var cursorPosition = process ? process(tr) : -1;
	        if (cursorPosition != -1) {
	            tr.setSelection(this.PM_.state.TextSelection.create(this.doc_, cursorPosition));
	        }
	        var newState = this.editorView.state.apply(tr);
	        this.editorView.updateState(newState);
	        if (cursorPosition != -1) {
	            this.editorView.focus();
	        }
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.getPositionInDocument_ = function (theNode) {
	        var pos = 0;
	        var finished = false;
	        this.doc_.descendants(function (node, p) {
	            if (finished) {
	                return false;
	            }
	            if (node.eq(theNode)) {
	                pos = p + 1;
	                finished = true;
	                return false;
	            }
	            return true;
	        });
	        return pos;
	    };
	    // noinspection JSMethodCanBeStatic
	    BeyondGrammarProseMirrorPlugin.prototype.rangeFromTransform_ = function (tr) {
	        var from, to;
	        for (var i = 0; i < tr.steps.length; i++) {
	            var step = tr.steps[i];
	            var stepMapping = step.getMap();
	            //new position after step
	            var stepFrom = stepMapping.map(step.from || step.pos, -1);
	            var stepTo = stepMapping.map(step.to || step.pos, 1);
	            if (from) {
	                from = Math.min(stepMapping.map(from, -1), stepFrom);
	            }
	            else {
	                from = stepFrom;
	            }
	            if (to) {
	                to = Math.max(stepMapping.map(to, 1), stepTo);
	            }
	            else {
	                to = stepTo;
	            }
	        }
	        return new dom_1.DocRange_(from, to);
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.getDecoById_ = function (uuid) {
	        var decos = this.decos_.find(null, null, function (spec) { return spec.id == uuid; });
	        return decos[0];
	    };
	    /**
	     * Methods stubbed for awhile
	     */
	    BeyondGrammarProseMirrorPlugin.prototype.bindEditable = function () {
	        this.isBound_ = true;
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.unbindEditable = function () {
	        this.isBound_ = false;
	    };
	    BeyondGrammarProseMirrorPlugin.prototype.bindChangeEvents = function () { };
	    BeyondGrammarProseMirrorPlugin.prototype.unbindChangeEvents = function () { };
	    BeyondGrammarProseMirrorPlugin.prototype.updateAfterPaste = function () { };
	    BeyondGrammarProseMirrorPlugin.prototype.resetSpellCheck = function () { };
	    BeyondGrammarProseMirrorPlugin.prototype.restoreSpellCheck = function () { };
	    return BeyondGrammarProseMirrorPlugin;
	}());
	exports.BeyondGrammarProseMirrorPlugin = BeyondGrammarProseMirrorPlugin;
	//Extending BeyondGrammar namespace
	window["BeyondGrammar"] = window["BeyondGrammar"] || {};
	window["BeyondGrammar"].createBeyondGrammarPluginSpec = createBeyondGrammarPluginSpec_;


/***/ },
/* 1 */,
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var editable_wrapper_1 = __webpack_require__(3);
	var uuid_1 = __webpack_require__(4);
	var HighlightSpec = (function () {
	    function HighlightSpec(tag, word, ignored) {
	        if (ignored === void 0) { ignored = false; }
	        this.tag = tag;
	        this.word = word;
	        this.ignored = ignored;
	        this.inclusiveStart = true;
	        this.inclusiveEnd = true;
	        this.id = 'pwa-' + uuid_1.uuid_();
	        this.highlightInfo = new editable_wrapper_1.HighlightInfo();
	        this.highlightInfo.category = tag.category;
	        this.highlightInfo.hint = tag.hint;
	        this.highlightInfo.suggestions = tag.suggestions;
	        this.highlightInfo.word = word;
	    }
	    return HighlightSpec;
	}());
	exports.HighlightSpec = HighlightSpec;


/***/ },
/* 3 */
/***/ function(module, exports) {

	///<reference path="../../rangy.d.ts"/>
	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var HighlightInfo = (function () {
	    function HighlightInfo() {
	    }
	    return HighlightInfo;
	}());
	exports.HighlightInfo = HighlightInfo;


/***/ },
/* 4 */
/***/ function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	/**
	 * Generates unique id
	 * @return {string}
	 */
	function uuid_() {
	    var res = '';
	    for (var i = 0; i < 32; i++) {
	        res += Math.floor(Math.random() * 16).toString(16).toUpperCase();
	    }
	    return res;
	}
	exports.uuid_ = uuid_;


/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function objectAssign(target, source) {
	    var attr = [];
	    for (var _i = 2; _i < arguments.length; _i++) {
	        attr[_i - 2] = arguments[_i];
	    }
	    var getOwnPropertySymbols = Object['getOwnPropertySymbols'];
	    var hasOwnProperty = Object.prototype.hasOwnProperty;
	    var propIsEnumerable = Object.prototype.propertyIsEnumerable;
	    var from;
	    var to = toObject(target);
	    var symbols;
	    for (var s = 1; s < arguments.length; s++) {
	        from = Object(arguments[s]);
	        for (var key in from) {
	            if (hasOwnProperty.call(from, key)) {
	                to[key] = from[key];
	            }
	        }
	        if (getOwnPropertySymbols) {
	            symbols = getOwnPropertySymbols(from);
	            for (var i = 0; i < symbols.length; i++) {
	                if (propIsEnumerable.call(from, symbols[i])) {
	                    to[symbols[i]] = from[symbols[i]];
	                }
	            }
	        }
	    }
	    return to;
	}
	exports.objectAssign = objectAssign;
	function toObject(val) {
	    if (val === null || val === undefined) {
	        throw new TypeError('Object.assign cannot be called with null or undefined');
	    }
	    return Object(val);
	}
	function createDecorationAttributesFromSpec(spec) {
	    // noinspection ReservedWordAsName
	    return {
	        class: "pwa-mark pwa-mark-done" + (spec.ignored ? " pwa-mark-ignored" : ""),
	        nodeName: "span",
	        "data-pwa-id": spec.id,
	        'data-pwa-category': spec.tag.category.toLowerCase(),
	        'data-pwa-hint': spec.tag.hint,
	        'data-pwa-suggestions': spec.tag.suggestions.join("~"),
	        'data-pwa-dictionary-word': spec.tag.text
	    };
	}
	exports.createDecorationAttributesFromSpec = createDecorationAttributesFromSpec;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	function getWindow_(el) {
	    if (document == el.ownerDocument) {
	        return window;
	    }
	    var doc = el.ownerDocument || document;
	    return doc.defaultView || doc.parentWindow;
	}
	exports.getWindow_ = getWindow_;
	function loadBeyondGrammarModule_(src, onLoad) {
	    var script = document.createElement("script");
	    script.src = src; //"bundle.js?r=" + Math.ceil(Math.random() * 1e6);
	    (document.head || document.body).appendChild(script);
	    var r = false;
	    script.onload = script["onreadystatechange"] = function () {
	        if (!r && (!this.readyState || this.readyState == 'complete')) {
	            r = true;
	            onLoad && onLoad(module = window["BeyondGrammar"]);
	        }
	    };
	}
	exports.loadBeyondGrammarModule_ = loadBeyondGrammarModule_;
	var DocRange_ = (function () {
	    function DocRange_(from, to) {
	        this.from = from;
	        this.to = to;
	    }
	    return DocRange_;
	}());
	exports.DocRange_ = DocRange_;

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(7)(module)))

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ }
/******/ ]);
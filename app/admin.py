from flask import redirect, url_for, request
from flask_admin import AdminIndexView as BaseAdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user


class AdminModelView(ModelView):

    def is_accessible(self):
        return current_user.is_administrator()

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('index', next=request.url))


class AdminUserModelView(AdminModelView):
        can_create = False  # disable model deletion
        column_filters = ['last_seen', 'member_since', 'confirmed']

class AdminDocModelView(AdminModelView):
    column_searchable_list = ['body', 'title']
    column_filters = ['author', 'created', 'edited']

    def get_query(self):
        print (self.model)
        return self.session.query(self.model).filter(self.model.title != 'Dictandu Tutorial').filter(self.model.author_id != 1)
 
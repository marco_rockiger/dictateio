#! conftest.py is a special file for fixtures with the pytest module
import os

import pytest

from app import create_app
from app import db as _db # to avoid name collision, rename db object 

DB_LOCATION = '/tmp/test_app.db'  # TODO use tempfile module, see tutorial


@pytest.fixture(scope='session')
def app():
    test_config = {
        'SECRET_KEY': 'test',
        'DB_LOCATION': DB_LOCATION,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///' + DB_LOCATION,
        'DEBUG': True,
        'TESTING': True
    }
    app = create_app(test_config=test_config)
    return app


@pytest.fixture(scope='session')
def db(app, request):
    """ Session-wide test database. """
    if os.path.exists(DB_LOCATION):
        os.unlink(DB_LOCATION)

    _db.app = app
    _db.create_all() # TODO use alembic the resemble migrate process
    # http://alexmic.net/flask-sqlalchemy-pytest/#migrations

    def teardown():
        _db.drop_all()
        os.unlink(DB_LOCATION)
    
    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='function')
def session(db, request):

    session = db.create_scoped_session()
    db.session = session

    def teardown():
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()

class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='test', password='test'):
        return self._client.post(
            'auth/login',
            data={'username': username, 'password': password}
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def auth(client):
    return AuthActions(client)
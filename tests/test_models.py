from app.models import User, Doc, AnonymousUser, Permission


def test_create_user_instance(session):
    """Create and save a user instance."""

    username = 'testuser'
    password = 'foobarbaz'

    user = User(username=username, password=password)
    session.add(user)
    session.commit()

    # We clear out the database after every run of the test suite
    # but the order of tests may affect which ID is assigned.
    # Let's not depend on magic numbers if we can avoid it.
    assert user.id is not None


def test_create_post_model(session):
    """Create and save a model instance"""

    username = 'author'
    password = 'foobarbaz'

    user = User(username=username, password=password)
    session.add(user)
    session.commit()

    title = 'Test Title'
    body = """Auch gibt es niemanden, der den Schmerz an sich liebt, sucht oder wünscht, nur, weil er Schmerz ist, es sei denn, es kommt zu zufälligen Umständen, in denen Mühen und Schmerz ihm große Freude bereiten können. Um ein triviales Beispiel zu nehmen, wer von uns unterzieht sich je anstrengender körperlicher Betätigung, außer um Vorteile daraus zu ziehen? Aber wer hat irgend ein Recht, einen Menschen zu tadeln, der die Entscheidung trifft, eine Freude zu."""

    post = Doc(title=title, body=body, author=user)
    session.add(post)
    session.commit()

    assert post.id > 0

def test_model_relationships(session):
    pass

def test_user_role():
    u = User(email='john@example.com', password='cat')
    assert u.can(Permission.DICTATE) is False
    assert u.can(Permission.MODERATE) is False
    assert u.can(Permission.ADMIN) is False

def test_anonymous_user():
    u = AnonymousUser()
    assert u.can(Permission.DICTATE) is False
    assert u.can(Permission.MODERATE) is False
    assert u.can(Permission.ADMIN) is False

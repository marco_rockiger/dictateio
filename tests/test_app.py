import flask

from app import create_app

#! the app argument is return value of the fixture, with 
#! the same name in conftest.py.
def test_app(app):
    assert isinstance(app, flask.Flask)

def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing
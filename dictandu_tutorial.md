In this tutorial we will show you, how you get the most out of Dictandu. We will guide you through the usage of the Dictandu.

You will learn how you use dictation to your advantage.

You will learn how you improve your writing.

And you will learn how you get the most out of the Editors capabilities.

It has worked best for us, if we stick to 2-step process: dictate, review, repeat.

The rest of this tutorial will it show you:

* how you dictate

* and how you edit your article.

## Dictation

To start the dictation click on “Start Recording” and then speak your sentences. It is best you use a headset with a stationary microphone, like the people in call centers use. To test Dictandu a standard headset for your phone or even the microphone on your laptop will do. The better the microphone the better the dictation result will be.

Now let's try it. Select the uppercase paragraph below, click on “Start Recording” and talk into your microphone.

SELECT ME

You can use these phrases to add punctuation to your text:

* Period

* Comma

* Exclamation point

* Question mark

* New paragraph

### Tips for successful dictation

#### Speak Fluently

To get the best results with dictation try to speak fluently and in whole sentences. Try to avoid breaks and let the words flow. This way it is easier for the speech recognition to get the meaning of your sentence.

#### Get your words out and keep your lips moving

This takes the same line as the tip before. Keep your lips moving. Do not edit the text while you're dictating! Worry about errors and style when you are editing your draft.

#### Create a structure first

Before you dictate your draft try to create sketch a structure of your text first. This way you don't have to think about the structure doing dictation and will dictate more fluently.

When you have a structure in place, you only have to fill in the gaps in your text.

We will soon add an extra feature for structuring that to Sarah.

#### Use placeholder words

Some words a very hard to understand for text recognition software. Text recognition software especially misunderstands made up names like Dictandu because it doesn’t know these words exist.

For this case use common names as a placeholder you can search and replace later in the editing step. For Example I used John instead of Dictandu to dictate this tutorial.

*We will soon add a feature for automatic word replacement.*

## Editing

If you are like most authors, you will probably have errors in your first draft.  This is why Dictandu supports you with your text editing.  Dictandu will assist you with:

* Spelling

* Grammar

* Style

### Writing Aid

Whenever we detect something we think you could improve, we will underline this part in the text. Hover over the underlined word with your mouse, to see the suggestions. Now let's look at some examples:

#### Spelling Mistakes

Spelling mistakes are probably the most obvous ones and we underline them red.

#### Grammar

While the English language has no strict punctuation rules, it is often easier for your reader if you add commas to show the structure of your sentence.

Grammatical errors often happen when you having been writing fast.

Dictandu underlines grammar errors in blue.

#### Style

Style is often a slippery slope. There is no definite answer to what is good and to what is bad style. On the other hand it cannot hurt to have somebody pointing out areas where we could improve. Style suggestions are underlined yellow.

### Editor Functions

Besides the writing aid, Dictandu has also standard editing functions to structure your text. They behave like you are used to from any text editor you know.

You can structure your text in headlines, lists and paragraphs. See the help section for keyboard shortcuts.

### Publish

When you finished your article and want to publish it, click on the “Export” button on the left, then copy the generated HTML code. Insert the HTML code in your WordPress installation or any other blog system.
